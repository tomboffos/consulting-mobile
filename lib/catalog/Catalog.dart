import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/catalog/CatalogSearch.dart';
import 'package:consulting/consultants/ConsultantsCatalog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class CatalogMain extends StatefulWidget {
  final categoryId;

  const CatalogMain({Key key, this.categoryId}) : super(key: key);

  @override
  _CatalogMainState createState() => _CatalogMainState();
}

class _CatalogMainState extends State<CatalogMain> {
  List<dynamic> categories = [];

  fetchCategories() async {
    final response = await get(Uri.parse('$apiEndPoint/category'));
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      setState(() {
        categories = body['data'];
      });
    }
  }

  @override
  void initState() {
    fetchCategories();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Каталог',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          GestureDetector(
            onTap: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => CatalogSearch())),
            child: Container(
              margin: EdgeInsets.only(right: 20),
              child: Icon(
                Icons.search,
                color: Color(0xff146C99),
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: ListView.builder(
        itemBuilder: (context, index) => GestureDetector(
          onTap: () => categories[index]['child'].length > 0
              ? pushNewScreen(context,
                  screen: CatalogMain(
                    categoryId: categories[index]['id'],
                  ))
              : pushNewScreen(context, screen: ConsultantsCatalog(category: categories[index],)),
          child: Container(
            width: width,
            padding: EdgeInsets.only(top: 20, bottom: 20, left: 12, right: 12),
            decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color(0xffE2E2E2), width: 1))),
            child: Text(
              categories[index]['name'],
              style: GoogleFonts.openSans(
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: Color(0xff191919)),
            ),
          ),
        ),
        itemCount: categories.length,
      ),
    );
  }
}
