import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/catalog/Catalog.dart';
import 'package:consulting/consultants/ConsultantsCatalog.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class CatalogSearch extends StatefulWidget {
  @override
  _CatalogSearchState createState() => _CatalogSearchState();
}

class _CatalogSearchState extends State<CatalogSearch> {
  List<dynamic> categories = [];
  TextEditingController search = new TextEditingController();
  searchCategory() async {
    final response =
        await get(Uri.parse('$apiEndPoint/category?search=${search.text}'));
    final body = jsonDecode(response.body);

    if (response.statusCode == 200)
      setState(() {
        categories = body['data'];
      });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Container(
          margin: EdgeInsets.only(top: 20),
          decoration: BoxDecoration(color: Colors.white),
          child: Column(
            children: [
              TextField(
                style: GoogleFonts.openSans(
                    color: Color(0xff191919),
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
                decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 12, vertical: 20),
                    suffixIcon: GestureDetector(
                      child: Icon(Icons.close),
                      onTap: () {
                        setState(() {
                          search.text = '';
                        });
                      },
                    )),
                controller: search,
                onChanged: (value) => searchCategory(),
              ),
              Container(
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) => GestureDetector(
                    onTap: () => categories[index]['child'].length > 0
                        ? pushNewScreen(context,
                            screen: CatalogMain(
                              categoryId: categories[index]['id'],
                            ))
                        : pushNewScreen(context,
                            screen: ConsultantsCatalog(
                              category: categories[index],
                            )),
                    child: Container(
                      width: width,
                      padding: EdgeInsets.only(
                          top: 20, bottom: 20, left: 12, right: 12),
                      decoration: BoxDecoration(
                          border: Border(
                              top: BorderSide(
                                  color: Color(0xffE2E2E2), width: 1))),
                      child: Text(
                        categories[index]['name'],
                        style: GoogleFonts.openSans(
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: Color(0xff191919)),
                      ),
                    ),
                  ),
                  itemCount: categories.length,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
