import 'package:awesome_dialog/awesome_dialog.dart';

showErrorModal(context,message){
  AwesomeDialog(
    context: context,
    dialogType: DialogType.ERROR,
    animType: AnimType.BOTTOMSLIDE,
    title: 'Ошибка',
    desc: message,
    useRootNavigator: true,
    btnOkOnPress: () {},
  )..show();
}

showSuccessModel(context,message){
  AwesomeDialog(
    context: context,
    dialogType: DialogType.SUCCES,
    animType: AnimType.BOTTOMSLIDE,
    title: 'Успешно',
    desc: message,
    useRootNavigator: true,
    btnOkOnPress: () {},
  )..show();
}