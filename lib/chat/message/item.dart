import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MessageItem extends StatefulWidget {
  final bool isOdd;
  final message;

  const MessageItem({Key key, this.isOdd, this.message}) : super(key: key);
  @override
  _MessageItemState createState() => _MessageItemState();
}

class _MessageItemState extends State<MessageItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: 10),
      decoration: BoxDecoration(
          color: widget.isOdd ? Color(0xffD6F0FD) : Color(0xffEAFFF4),
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(widget.isOdd ? 0 : 15),
            topRight: Radius.circular(15),
            topLeft: Radius.circular(15),
            bottomRight: Radius.circular(widget.isOdd ? 15 : 0),
          )),
      padding: EdgeInsets.only(
          top: 12,
          bottom: 12,
          left: widget.isOdd ? 10 : 15,
          right: widget.isOdd ? 15 : 10),
      margin: EdgeInsets.only(
          bottom: 15,
          left: widget.isOdd ? 0 : 22,
          right: widget.isOdd ? 22 : 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.message['body']),
          SizedBox(
            height: 4,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                '${widget.message['created_at']}',
                style: GoogleFonts.openSans(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xffA3A3A3),
                ),
                textAlign: TextAlign.right,
              ),
            ],
          )
        ],
      ),
    );
  }
}
