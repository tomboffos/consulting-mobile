import 'package:consulting/chat/show.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class ChatItem extends StatefulWidget {
  final isEnded;
  final isConsultant;
  final chat;

  const ChatItem({Key key, this.isEnded, this.isConsultant, this.chat})
      : super(key: key);

  @override
  _ChatItemState createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  bool choose = false;

  @override
  void initState() {
    // TODO: implement initState
    choose = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        setState(() {
          choose = !choose;
        });
      },
      onTap: () => pushNewScreen(context,
          screen: ChatPage(
            isEnded: widget.isEnded,
            isConsultant: widget.isConsultant,
            chat: widget.chat,
          ),
          withNavBar: false),
      child: Container(
        decoration: BoxDecoration(
            color: choose ? Color(0xffE8F7FF) : Colors.white,
            border:
                Border(bottom: BorderSide(color: Color(0xffE2E2E2), width: 1))),
        child: Column(
          children: [
            Container(
              child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.grey,
                  foregroundImage: NetworkImage(
                      "${widget.chat['profile']['user']['avatar']}"),
                  radius: 25,
                ),
                title: Text(
                  widget.chat['profile']['user']['name'],
                  style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff0E0E0E)),
                ),
                subtitle: Text(
                  widget.chat['profile']['category']['name'],
                  style: GoogleFonts.openSans(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff191919)),
                ),
                trailing: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width / 2.2),
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: 5),
                            child: Text(
                              '${widget.chat['created_at']}',
                              style: GoogleFonts.openSans(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff717070)),
                            ),
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.end,
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width / 2.2),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          widget.chat['status']['id'] == 2
                              ? Container(
                                  padding: EdgeInsets.only(
                                      bottom: 4, left: 10, right: 10, top: 2),
                                  // margin: EdgeInsets.only(right: 16),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(
                                          color: Theme.of(context)
                                              .secondaryHeaderColor)),
                                  child: CountdownTimer(
                                    endTime:
                                        DateTime.parse(widget.chat['end_time'])
                                                .microsecondsSinceEpoch +
                                            1000 * 30,
                                    widgetBuilder:
                                        (_, CurrentRemainingTime time) {
                                      return Text(
                                        '${time.hours}:${time.min}',
                                        style: GoogleFonts.openSans(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Theme.of(context)
                                                .secondaryHeaderColor),
                                      );
                                    },
                                  ))
                              : SizedBox.shrink(),
                          widget.chat['status']['id'] == 3
                              ? Container(
                                  margin: EdgeInsets.only(bottom: 5),
                                  child: Text(
                                    '${widget.chat['status']['name']}',
                                    style: GoogleFonts.openSans(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xff717070)),
                                  ),
                                )
                              : SizedBox.shrink(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
