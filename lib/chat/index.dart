import 'dart:convert';

import 'package:adhara_socket_io/manager.dart';
import 'package:adhara_socket_io/options.dart';
import 'package:adhara_socket_io/socket.dart';
import 'package:consulting/auth/Start.dart';
import 'package:consulting/auth/client/register.dart';
import 'package:consulting/chat/item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

class ChatIndex extends StatefulWidget {
  final bool isConsultant;

  const ChatIndex({Key key, this.isConsultant}) : super(key: key);
  @override
  _ChatIndexState createState() => _ChatIndexState();
}

class _ChatIndexState extends State<ChatIndex> {
  Map<String, dynamic> user;

  checkToken() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    if (token != null) {
      final response = await get(Uri.parse('$apiEndPoint/user'),
          headers: {"Authorization": "Bearer $token"});
      final body = jsonDecode(response.body);
      setState(() {
        user = body['data'];
      });
    }
  }

  List<dynamic> chats;
  fetchChats() async {
    setState(() {
      chats = null;
    });
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(Uri.parse('$apiEndPoint/user/chats'),
        headers: {"Authorization": "Bearer $token"});
    print(token);
    if (response.statusCode == 200)
      setState(() {
        chats = jsonDecode(response.body)['data'];
      });
  }

  fetchConsultnantChats() async {
    setState(() {
      chats = null;
    });
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(Uri.parse('$apiEndPoint/consultant/chats'),
        headers: {"Authorization": "Bearer $token"});

    if (response.statusCode == 200)
      setState(() {
        chats = jsonDecode(response.body)['data'];
      });
  }

  SocketIO socket;
  linkSocket() async {
    await checkToken();
    print(user['id']);
    socket = await SocketIOManager()
        .createInstance(SocketOptions('https://socket.bhub.kz'));
    socket.onConnect.listen((data) {
      print('connected: $data');
    });
    await socket.connectSync();
    socket
        .on(
          'chat-message-${user['id']}',
        )
        .listen((event) => fetchChats());
    // widget.isConsultant ? fetchConsultnantChats() : fetchChats()
  }

  @override
  void initState() {
    // TODO: implement initState

    checkToken();
    if (widget.isConsultant)
      fetchConsultnantChats();
    else
      fetchChats();
    super.initState();
    // linkSocket();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Чаты',
          style: GoogleFonts.openSans(
              fontSize: 18,
              fontWeight: FontWeight.w700,
              color: Color(0xff191919)),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xff191919)),
        elevation: 1,
      ),
      backgroundColor: Colors.white,
      body: user == null
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    'Зарегистрируйтесь или войдите',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 20,
                  padding: EdgeInsets.symmetric(horizontal: 47),
                  margin: EdgeInsets.only(bottom: 18, top: 20),
                  child: RaisedButton(
                    onPressed: () => pushNewScreen(context,
                        screen: StartScreen(), withNavBar: false),
                    color: Theme.of(context).primaryColorDark,
                    child: Text(
                      'Войти',
                      style: GoogleFonts.roboto(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)),
                    padding: EdgeInsets.symmetric(vertical: 16),
                  ),
                )
              ],
            )
          : chats == null
              ? Center(child: CircularProgressIndicator())
              : Container(
                  child: RefreshIndicator(
                  onRefresh: () => fetchChats(),
                  child: ListView.builder(
                    itemBuilder: (context, index) => ChatItem(
                        isEnded: true,
                        isConsultant: widget.isConsultant,
                        chat: chats[index]),
                    itemCount: chats.length,
                  ),
                )),
    );
  }
}
