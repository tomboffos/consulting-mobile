import 'dart:convert';

import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:consulting/auth/client/register.dart';
import 'package:consulting/chat/call/audio.dart';
import 'package:consulting/chat/call/video.dart';
import 'package:consulting/chat/message/item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io_client/socket_io_client.dart';
import 'package:web_socket_channel/io.dart';

class ChatPage extends StatefulWidget {
  final bool isEnded;
  final chat;
  final bool isConsultant;
  const ChatPage({Key key, this.isEnded, this.isConsultant, this.chat})
      : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  List<dynamic> messages;

  void fetchChat() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    print(token);
    print(widget.chat['chat']['id']);
    final response = await get(
        Uri.parse('$apiEndPoint/user/chats/${widget.chat['chat']['id']}'),
        headers: {"Authorization": "Bearer $token"});
    print(response.body);
    setState(() {
      messages = jsonDecode(response.body)['data'];
    });
  }

  TextEditingController body = new TextEditingController();
  void storeChat() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await post(Uri.parse('$apiEndPoint/user/message'),
        headers: {
          "Authorization": "Bearer $token",
          "Accept": "application/json"
        },
        body: {
          "body": body.text,
          "chat_id": widget.chat['chat']['id'].toString()
        });
    setState(() {
      body.text = '';
    });
    print(jsonDecode(response.body));
    fetchChat();
  }

  Map<String, dynamic> user;
  checkToken() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    if (token != null) {
      final response = await get(Uri.parse('$apiEndPoint/user'),
          headers: {"Authorization": "Bearer $token"});
      final body = jsonDecode(response.body);
      setState(() {
        user = body['data'];
      });
    }
  }

  SocketIO socket;
  linkSocket() async {
    socket = await SocketIOManager()
        .createInstance(SocketOptions('https://socket.bhub.kz'));
    socket.onConnect.listen((data) {
      print('connected: $data');
    });
    socket.onConnectError.toString();
    await socket.connectSync();
    socket
        .on(
          'user-chat-event-${widget.chat['chat']['id']}',
        )
        .listen((event) => fetchChat());
    // widget.isConsultant ? fetchConsultnantChats() : fetchChats()
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchChat();
    checkToken();
    linkSocket();
    super.initState();
    // linkSocket();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(right: 10),
              child: CircleAvatar(
                backgroundColor: Colors.grey,
                foregroundImage:
                    NetworkImage("${widget.chat['profile']['user']['avatar']}"),
                radius: 16,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${widget.chat['profile']['user']['name']}',
                  style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff0E0E0E)),
                ),
              ],
            ),
          ],
        ),
        iconTheme: IconThemeData(color: Colors.black),
        actions: [
          GestureDetector(
            onTap: () => pushNewScreen(
              context,
              screen: AudioChat(
                user: !widget.isConsultant
                    ? widget.chat['profile']['user']
                    : user,
              ),
            ),
            child: Container(
              child: Icon(
                Icons.phone,
                color: widget.isConsultant
                    ? Theme.of(context).secondaryHeaderColor
                    : Theme.of(context).primaryColor,
              ),
            ),
          ),
          GestureDetector(
            onTap: () => pushNewScreen(
              context,
              screen: VideoChat(),
            ),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              child: Icon(
                Icons.videocam,
                color: widget.isConsultant
                    ? Theme.of(context).secondaryHeaderColor
                    : Theme.of(context).primaryColor,
              ),
            ),
          ),
        ],
        elevation: 1,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 12, bottom: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Icon(
                      Icons.schedule,
                      size: 20,
                    ),
                    margin: EdgeInsets.only(right: 6),
                  ),
                  Text(
                    widget.chat['status']['id'] == 2
                        ? 'Ваш чат будет закрыт через'
                        : widget.isConsultant
                            ? 'Ваш урок закончена'
                            : 'Ваша консультация окончена',
                    style: GoogleFonts.openSans(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff050505),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    margin: EdgeInsets.only(left: 6),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                          color: widget.chat['status']['id'] == 2
                              ? Theme.of(context).secondaryHeaderColor
                              : Colors.grey),
                    ),
                    child: CountdownTimer(
                      endTime: DateTime.parse(widget.chat['end_time'])
                              .microsecondsSinceEpoch +
                          1000 * 60,
                      widgetBuilder: (_, CurrentRemainingTime time) => Text(
                        widget.chat['status']['id'] == 2
                            ? '${time.hours}:${time.min}'
                            : '00:00',
                        style: GoogleFonts.openSans(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: widget.chat['status']['id'] == 2
                                ? Theme.of(context).secondaryHeaderColor
                                : Colors.grey),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height / 1.36,
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: user == null || messages == null
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : ListView.builder(
                      reverse: true,
                      itemBuilder: (context, index) => MessageItem(
                        isOdd: !(user['id'] == messages[index]['user_id']),
                        message: messages[index],
                      ),
                      itemCount: messages.length,
                    ),
            ),
            widget.chat['status']['id'] == 2
                ? Container(
                    height: 80,
                    padding: EdgeInsets.only(
                        bottom: 20, left: 14, right: 14, top: 20),
                    decoration: BoxDecoration(color: Colors.white, boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          spreadRadius: 10,
                          offset: Offset(0, 1),
                          blurRadius: 10)
                    ]),
                    child: Row(
                      children: [
                        // Container(
                        //   height: 20,
                        //   child: Icon(
                        //     Icons.attach_file,
                        //     color: Color(0xff7A7A7A),
                        //   ),
                        // ),
                        Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          height: 20,
                          width: MediaQuery.of(context).size.width / 1.3,
                          child: TextField(
                            onSubmitted: (value) => storeChat(),
                            controller: body,
                            style: GoogleFonts.openSans(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Colors.black),
                            decoration: InputDecoration(
                                hintText: 'Сообщение...',
                                hintStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff7A7A7A)),
                                contentPadding: EdgeInsets.all(0),
                                disabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white))),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => storeChat(),
                          child: Container(
                            child: Icon(
                              Icons.send,
                              color: Color(0xff1D9CDD),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                : Container(
                    height: 80,
                    padding: EdgeInsets.only(
                        bottom: 10, left: 14, right: 14, top: 10),
                    decoration: BoxDecoration(
                        color: widget.isConsultant
                            ? Theme.of(context).secondaryHeaderColor
                            : Theme.of(context).primaryColor),
                    child: Center(
                      child: Text(
                        widget.isConsultant
                            ? 'Нужна ли вам повторная консультация ?'
                            : 'Консультация окончена',
                        style: GoogleFonts.openSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
