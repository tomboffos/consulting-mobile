import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:consulting/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;

class VideoChat extends StatefulWidget {
  @override
  _VideoChatState createState() => _VideoChatState();
}

class _VideoChatState extends State<VideoChat> {
  RtcEngine _engine;
  Future<void> _handleCameraAndMic(Permission permission) async {
    final status = await permission.request();
    print(status);
  }

  bool videOffed = false;
  bool micOffed = false;

  initForAgora() async {
    await _handleCameraAndMic(Permission.camera);
    await _handleCameraAndMic(Permission.microphone);
    _engine = await RtcEngine.createWithConfig(RtcEngineConfig(appId));

    await _engine.enableVideo();

    _engine.setEventHandler(RtcEngineEventHandler(
        joinChannelSuccess: (String channel, int uid, int elapsed) {
      print('local user $uid joined');
    }, userJoined: (int uid, int elapsed) {
      print('another user $uid joined');
      setState(() {
        _remoteUid = uid;
      });
    }, userOffline: (int uid, UserOfflineReason reason) {
      print("remote user $uid left channel");

      _engine.destroy();
      Navigator.pop(context);
    }));

    await _engine.joinChannel(token, "new", null, 0);
  }

  Future<void> disconnect() async {
    await _engine.destroy();
    Navigator.pop(context);
  }

  Widget _renderLocalPreview() {
    return RtcLocalView.SurfaceView();
  }

  int _remoteUid;
  Widget _renderRemoteVideo() {
    if (_remoteUid != null) {
      return Container(
        child: RtcRemoteView.SurfaceView(
          uid: _remoteUid,
        ),
      );
    } else {
      return Text('Пожалуйста подождите');
    }
  }

  videoChange() {
    if (videOffed)
      _engine.enableVideo();
    else
      _engine.disableVideo();

    setState(() {
      videOffed = !videOffed;
    });
  }

  audioChange() {
    if (micOffed)
      _engine.enableAudio();
    else
      _engine.disableAudio();
    setState(() {
      micOffed = !micOffed;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    initForAgora();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Center(
          child: _renderRemoteVideo(),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Container(
            width: 200,
            height: 200,
            child: Center(
              child: !videOffed ? _renderLocalPreview() : SizedBox.shrink(),
            ),
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Container(
            margin: EdgeInsets.only(top: 200),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FloatingActionButton(
                  onPressed: () => audioChange(),
                  backgroundColor: Theme.of(context).primaryColor,
                  child: Icon(
                    micOffed ? Icons.volume_mute : Icons.volume_up,
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  width: 32,
                ),
                Container(
                  child: FloatingActionButton(
                    backgroundColor: Colors.red,
                    onPressed: () => disconnect(),
                    child: Icon(
                      Icons.call_end,
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(
                  width: 32,
                ),
                FloatingActionButton(
                  onPressed: () => videoChange(),
                  backgroundColor: Theme.of(context).primaryColor,
                  child: Icon(
                    videOffed ? Icons.videocam_off : Icons.video_call,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
