import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../main.dart';

class AudioChat extends StatefulWidget {
  final user;

  const AudioChat({Key key, this.user}) : super(key: key);
  @override
  _AudioChatState createState() => _AudioChatState();
}

class _AudioChatState extends State<AudioChat> {
  RtcEngine _engine;
  int _remoteUid;
  bool videOffed = false;
  bool micOffed = false;

  audioChange() {
    if (micOffed)
      _engine.enableAudio();
    else
      _engine.disableAudio();
    setState(() {
      micOffed = !micOffed;
    });
  }

  Future<void> _handleCameraAndMic(Permission permission) async {
    final status = await permission.request();
    print(status);
  }

  initForAgora() async {
    await _handleCameraAndMic(Permission.microphone);
    _engine = await RtcEngine.createWithConfig(RtcEngineConfig(appId));

    await _engine.enableVideo();

    _engine.setEventHandler(RtcEngineEventHandler(
        joinChannelSuccess: (String channel, int uid, int elapsed) {
      print('local user $uid joined');
    }, userJoined: (int uid, int elapsed) {
      print('another user $uid joined');
      setState(() {
        _remoteUid = uid;
      });
    }, userOffline: (int uid, UserOfflineReason reason) {
      print("remote user $uid left channel");
      setState(() {
        _remoteUid = null;
      });
      _engine.destroy();
      Navigator.pop(context);
    }));

    await _engine.joinChannel(token, "new", null, 0);
  }

  Future<void> disconnect() async {
    await _engine.destroy();
    Navigator.pop(context);
  }

  @override
  void initState() {
    // TODO: implement initState
    initForAgora();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage('${widget.user['avatar']}'))),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(top: 60),
                child: Text(
                  'Аудиозвонок',
                  style: GoogleFonts.openSans(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff191919)),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Text(
                  '${widget.user['name']}',
                  style: GoogleFonts.openSans(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      color: Color(0xff191919)),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [],
                ),
              ),
              SizedBox(
                height: 250,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: FloatingActionButton(
                      backgroundColor: Colors.red,
                      onPressed: () => disconnect(),
                      child: Icon(
                        Icons.call_end,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 32,
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => audioChange(),
                      backgroundColor: Theme.of(context).primaryColor,
                      child: Icon(
                        micOffed ? Icons.mic_none : Icons.mic,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
