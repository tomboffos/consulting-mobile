import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:consulting/components/modals.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class PaymentPage extends StatefulWidget {
  final data;

  const PaymentPage({Key key, this.data}) : super(key: key);

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  bool isEnded = false;

  urlChangeHandler() {
    flutterWebViewPlugin.onUrlChanged.listen((String url) {
      print(url);
      if(url.contains("https://consulting.bhub.kz/profile/success")){
        setState(() {
          isEnded = true;
        });
        AwesomeDialog(
            context: context,
            borderSide: BorderSide(color: Colors.green, width: 2),
            width: 500,
            buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
            headerAnimationLoop: true,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Успешно',
            desc: 'Оплата прошла успешно',
            showCloseIcon: false,
            useRootNavigator: true,
            dialogType: DialogType.SUCCES,
            btnOkOnPress: () {})
          ..show();
      }

      if (url.contains('https://consulting.bhub.kz/consultation/success')) {
        setState(() {
          isEnded = true;
        });
        AwesomeDialog(
            context: context,
            borderSide: BorderSide(color: Colors.green, width: 2),
            width: 500,
            buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
            headerAnimationLoop: true,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Успешно',
            desc: 'Ваша консультация начнется скоро',
            showCloseIcon: false,
            useRootNavigator: true,
            dialogType: DialogType.SUCCES,
            btnOkOnPress: () {})
          ..show();
      } else if (url.contains('https://consulting.bhub.kz/consultation/fail'))
        showErrorModal(context, 'Платеж к сожалению не пройден');
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    urlChangeHandler();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 1,
        title: Text(
          'Оплата консультации',
          style: Theme.of(context).textTheme.headline6,
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: isEnded
          ? Container()
          : WebviewScaffold(
              url: new Uri.dataFromString(this.widget.data,
                      mimeType: 'text/html')
                  .toString()),
    );
  }
}
