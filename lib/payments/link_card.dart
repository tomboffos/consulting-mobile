import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:google_fonts/google_fonts.dart';

class LinkCardWebView extends StatefulWidget {
  final link;
  const LinkCardWebView({Key key, this.link}) : super(key: key);

  @override
  _LinkCardWebViewState createState() => _LinkCardWebViewState();
}

class _LinkCardWebViewState extends State<LinkCardWebView> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  bool linkEnded = false;
  urlChange(){
    flutterWebViewPlugin.onUrlChanged.listen((String url) {
      print(url);
      if(url.contains('https://consulting.bhub.kz')){
        setState(() {
          linkEnded = true;
        });
        Navigator.pop(context);
      }
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    urlChange();

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 1,
        title: Text(
          'Мои карты', style: GoogleFonts.openSans(
            color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body:linkEnded ? Container() : WebviewScaffold(
        url: '${this.widget.link}',
      ),
    );
  }
}
