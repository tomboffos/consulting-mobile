import 'package:consulting/filter/items.dart';
import 'package:consulting/filter/price.dart';
import 'package:consulting/filter/rating.dart';
import 'package:consulting/filter/time.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class FilterIndex extends StatefulWidget {
  @override
  _FilterIndexState createState() => _FilterIndexState();
}

class _FilterIndexState extends State<FilterIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Фильтры',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: GestureDetector(
        child: Container(
          child: Column(
            children: [
              GestureDetector(
                onTap: () => {
                  pushNewScreen(
                    context,
                    screen: RatingFilter(),
                    withNavBar: false, // OPTIONAL VALUE. True by default.
                    pageTransitionAnimation: PageTransitionAnimation.cupertino,
                  )
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 12),
                      child: Text(
                        'Рейтинг',
                        style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    Divider(),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () => pushNewScreen(
                  context,
                  screen: PriceFilter(),
                  withNavBar: false, // OPTIONAL VALUE. True by default.
                  pageTransitionAnimation: PageTransitionAnimation.cupertino,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 12),
                      child: Text(
                        'Цена',
                        style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    Divider(),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () => pushNewScreen(
                  context,
                  screen: TimeFilter(),
                  withNavBar: false, // OPTIONAL VALUE. True by default.
                  pageTransitionAnimation: PageTransitionAnimation.cupertino,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 12),
                      child: Text(
                        'Свободное время',
                        style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    Divider(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
