import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PriceFilter extends StatefulWidget {
  @override
  _PriceFilterState createState() => _PriceFilterState();
}

class _PriceFilterState extends State<PriceFilter> {
  var filterValue;
  List<String> prices;
  savePrices() async {
    (await SharedPreferences.getInstance()).setStringList('prices', prices);
  }

  getPrices() async {
    prices = (await SharedPreferences.getInstance()).getStringList('prices');
    if (prices != null) {
      setState(() {
        if ((prices[0] == '2500' && prices[1] == '5000') ||
            (prices[0] == '5000' && prices[1] == '10000')) {
          filterValue = prices.toString();
          print(filterValue);
        } else {
          startPrice.text = prices[0];
          endPrice.text = prices[1];
        }
      });
    }
  }

  TextEditingController startPrice = new TextEditingController();
  TextEditingController endPrice = new TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    getPrices();
    filterValue = 'all';
    super.initState();
  }

  removePrices() async {
    setState(() {
      filterValue = 'all';
      startPrice.text = '';
      endPrice.text = '';
    });

    (await SharedPreferences.getInstance()).remove('prices');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Цена',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w600),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () => removePrices(),
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 20),
              child: Text(
                'Сбросить',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff575757)),
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: TextField(
                      onTap: () => setState(() => filterValue = 'all'),
                      controller: startPrice,
                      onEditingComplete: () {
                        if (endPrice.text != '') {
                          prices = [startPrice.text, endPrice.text];
                          savePrices();
                        }
                      },
                      textAlign: TextAlign.center,
                      style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff191919)),
                      decoration: InputDecoration(
                        prefix: Text(
                          'от',
                          style: GoogleFonts.openSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Colors.grey),
                        ),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xff545454),
                            ),
                            borderRadius: BorderRadius.circular(0)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xff545454),
                            ),
                            borderRadius: BorderRadius.circular(0)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xff545454),
                            ),
                            borderRadius: BorderRadius.circular(0)),
                      ),
                    ),
                    width: MediaQuery.of(context).size.width / 2,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: TextField(
                      onTap: () => setState(() => filterValue = 'all'),
                      onEditingComplete: () {
                        setState(() {
                          if (startPrice.text != '') {
                            prices = [startPrice.text, endPrice.text];
                            savePrices();
                          }

                          filterValue = 'all';
                        });
                      },
                      controller: endPrice,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff191919)),
                      decoration: InputDecoration(
                        prefix: Text(
                          'до',
                          style: GoogleFonts.openSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Colors.grey),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xff545454),
                            ),
                            borderRadius: BorderRadius.circular(0)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xff545454),
                            ),
                            borderRadius: BorderRadius.circular(0)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xff545454),
                            ),
                            borderRadius: BorderRadius.circular(0)),
                      ),
                    ),
                    width: MediaQuery.of(context).size.width / 2,
                  )
                ],
              ),
            ),
            CheckboxListTile(
              title: Text(
                'от 2500 тг до 5000 тг',
                style: GoogleFonts.openSans(
                    fontSize: 16, fontWeight: FontWeight.w400),
              ),
              value: filterValue == '[2500, 5000]',
              onChanged: (value) {
                setState(() {
                  filterValue = '[2500, 5000]';
                  prices = ['2500', '5000'];
                  savePrices();
                });
              },
              activeColor: Theme.of(context).primaryColor,
            ),
            Divider(),
            CheckboxListTile(
              title: Text(
                'от 5000 тг до 10000тг',
                style: GoogleFonts.openSans(
                    fontSize: 16, fontWeight: FontWeight.w400),
              ),
              value: filterValue == '[5000, 10000]',
              onChanged: (value) {
                setState(() {
                  filterValue = '[5000, 10000]';
                  prices = ['5000', '10000'];
                  savePrices();
                });
              },
              activeColor: Theme.of(context).primaryColor,
            ),
            Divider(),
          ],
        ),
      ),
    );
  }
}
