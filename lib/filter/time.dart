import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TimeFilter extends StatefulWidget {
  @override
  _TimeFilterState createState() => _TimeFilterState();
}

class _TimeFilterState extends State<TimeFilter> {
  var filterValue;

  @override
  void initState() {
    // TODO: implement initState
    filterValue = '11:00';
    fetchTimes();
    super.initState();
  }

  saveTime(String startTime, String endTime) async {
    (await SharedPreferences.getInstance())
        .setStringList('times', [startTime, endTime]);
  }

  deleteTimes() async {
    setState(() {
      filterValue = null;
    });
    (await SharedPreferences.getInstance()).remove('times');
  }

  fetchTimes() async {
    final time = (await SharedPreferences.getInstance()).getStringList('times');
    setState(() {
      if (time != null) {
        filterValue = DateTime.parse(time[0]).hour.toString();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'График',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w600),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () => deleteTimes(),
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 20),
              child: Text(
                'Сбросить',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff575757)),
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: [
              CheckboxListTile(
                title: Text(
                  '${DateTime.now().add(Duration(hours: index)).hour}:00 - ${DateTime.now().add(Duration(hours: index + 1)).hour}:00',
                  style: GoogleFonts.openSans(
                      fontSize: 16, fontWeight: FontWeight.w400),
                ),
                value: filterValue ==
                    '${DateTime.now().add(Duration(hours: index)).hour}',
                onChanged: (value) {
                  setState(() {
                    filterValue =
                        '${DateTime.now().add(Duration(hours: index)).hour}';
                  });

                  saveTime(
                      '${DateTime.now().add(Duration(hours: index)).toIso8601String()}',
                      '${DateTime.now().add(Duration(hours: index + 1)).toIso8601String()}');
                },
                activeColor: Theme.of(context).primaryColor,
              ),
              Divider(),
            ],
          );
        },
        itemCount: 24,
      ),
    );
  }
}
