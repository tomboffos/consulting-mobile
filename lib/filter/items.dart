import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FilterItem extends StatefulWidget {
  @override
  _FilterItemState createState() => _FilterItemState();
}

class _FilterItemState extends State<FilterItem> {
  var filterValue;

  @override
  void initState() {
    // TODO: implement initState
    filterValue = 'all';
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Предметы',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w600),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: (){
              setState(() {
                filterValue = null;
              });
            },
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 20),
              child: Text(
                'Сбросить',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff575757)),
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          CheckboxListTile(
            title: Text('Тригонометрия',style: GoogleFonts.openSans(
                fontSize: 16,
                fontWeight: FontWeight.w400
            ),),
            value: filterValue == 'all',
            onChanged: (value){
              setState(() {
                filterValue = 'all' ;
              });
            },
            activeColor: Theme.of(context).primaryColor,
          ),
          Divider(),
          CheckboxListTile(
            title: Text('Геометрия',style: GoogleFonts.openSans(
                fontSize: 16,
                fontWeight: FontWeight.w400
            ),),
            value: filterValue == 'second',
            onChanged: (value) {
              setState(() {
                filterValue = 'second';
              });
            },
            activeColor: Theme.of(context).primaryColor,
          ),
          Divider(),
        ],
      ),
    );
  }
}
