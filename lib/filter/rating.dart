import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RatingFilter extends StatefulWidget {
  @override
  _RatingFilterState createState() => _RatingFilterState();
}

class _RatingFilterState extends State<RatingFilter> {
  var filterValue = 'all';

  void saveRating() async {
    (await SharedPreferences.getInstance()).setString('ratings', '1');
    setState(() {
      filterValue = 'second';
    });
  }

  void removeRating() async {
    (await SharedPreferences.getInstance()).remove('ratings');
    setState(() {
      filterValue = 'all';
    });
  }

  void fetchRating() async {
    if ((await SharedPreferences.getInstance()).get('ratings') != null)
      setState(() {
        filterValue = 'second';
      });
  }

  @override
  void initState() {
    fetchRating();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Рейтинг',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w600),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () => removeRating(),
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 20),
              child: Text(
                'Сбросить',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff575757)),
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          CheckboxListTile(
            title: Text(
              'Все услуги',
              style: GoogleFonts.openSans(
                  fontSize: 16, fontWeight: FontWeight.w400),
            ),
            value: filterValue == 'all',
            onChanged: (value) {
              removeRating();
            },
            activeColor: Theme.of(context).primaryColor,
          ),
          Divider(),
          CheckboxListTile(
            title: Text(
              '4 и 5 звезд',
              style: GoogleFonts.openSans(
                  fontSize: 16, fontWeight: FontWeight.w400),
            ),
            value: filterValue == 'second',
            onChanged: (value) {
              saveRating();
            },
            activeColor: Theme.of(context).primaryColor,
          ),
          Divider(),
        ],
      ),
    );
  }
}
