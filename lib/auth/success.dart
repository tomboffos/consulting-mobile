import 'package:consulting/consultant/ConsultantNavigation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SuccessPage extends StatefulWidget {
  @override
  _SuccessPageState createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>ConsultantNavigation()));
            },
            child: Container(
              margin: EdgeInsets.only(right: 20),
              alignment: Alignment.center,
              child: Text(
                'Пропустить',
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.only(top: 90),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
                child: Icon(
              Icons.check,
              color: Theme.of(context).secondaryHeaderColor,
              size: 100,
            )),
            SizedBox(
              height: 24,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                'Вы успешно прошли регистрацию создайте анкету или даже несколько и приступайте к консультациям',
                style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(child: Container()),
            Container(
                margin: EdgeInsets.only(bottom: 20),
                width: MediaQuery.of(context).size.width - 32,
                child: RaisedButton(
                  color: Theme.of(context).secondaryHeaderColor,

                  onPressed: () => {},
                  child: Text(
                    'Создать анкету',
                    style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                    ),

                  ),
                  padding: EdgeInsets.symmetric(vertical: 15),
                ))
          ],
        ),
      ),
    );
  }
}
