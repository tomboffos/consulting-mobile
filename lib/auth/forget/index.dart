import 'package:consulting/auth/login/approve.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ForgetIndex extends StatefulWidget {
  @override
  _ForgetIndexState createState() => _ForgetIndexState();
}

class _ForgetIndexState extends State<ForgetIndex> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        automaticallyImplyLeading: true,
        title: Text(
          'Укажите номер телефона',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        iconTheme: IconThemeData(
          color: Colors.black
        ),

      ),
      body: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(left: 15, right: 15),
        child: Column(
          children: [

            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Телефон',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: TextField(
                      style: Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: BorderSide(
                                color: Color(0xff545454),
                              )),
                          hintText: '+7 (777) 777-77-77',
                          hintStyle: Theme.of(context).textTheme.bodyText2),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(child: Container()),
            GestureDetector(
              onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Approve())),
              child: Container(
                margin: EdgeInsets.only(bottom: 10),
                width: width - 24,
                padding: EdgeInsets.symmetric(vertical: 15),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context).primaryColor
                    )
                ),
                child: Container(
                  child: Text(
                    'Продолжить',
                    style: GoogleFonts.openSans(
                        color: Theme.of(context).primaryColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );

  }
}
