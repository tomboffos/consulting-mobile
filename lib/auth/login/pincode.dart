import 'package:consulting/auth/forget/index.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'index.dart';


class PinCodeLogin extends StatefulWidget {
  @override
  _PinCodeLoginState createState() => _PinCodeLoginState();
}

class _PinCodeLoginState extends State<PinCodeLogin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 10),

            ),
            Center(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 72),
                child: Text(
                  'Введите код для быстрого доступа к приложению',
                  style: GoogleFonts.openSans(
                      fontSize: 14, fontWeight: FontWeight.w400),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            SizedBox(
              height: 100,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 6,
                  ),
                  CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 6,
                  ),
                  CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 6,
                  ),
                  CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 6,
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>ForgetIndex())),
              child: Container(
                margin: EdgeInsets.only(top: 40),
                child: Center(
                  child: Text(
                    'Забыл код доступа'
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40),
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Wrap(
                children: [
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('1',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 40,left: 40,bottom: 16),
                    child: FloatingActionButton(
                      onPressed: () => {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('2',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('3',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('4',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 40,left: 40,bottom: 16),
                    child: FloatingActionButton(
                      onPressed: () => {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('5',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('6',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('7',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 40,left: 40,bottom: 16),
                    child: FloatingActionButton(
                      onPressed: () => {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('8',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('9',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  SizedBox(
                    width: 55,
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 40,left: 40,bottom: 16),
                    child: FloatingActionButton(
                      onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginIndex())),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('0',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Icon(Icons.backspace,color: Theme.of(context).primaryColor,),
                      backgroundColor: Colors.white,
                    ),
                  ),


                ],
              ),
            )
          ],
        ),
      ),
    );

  }
}
