import 'dart:convert';

import 'package:consulting/auth/client/pincode.dart';
import 'package:consulting/auth/client/register.dart';
import 'package:consulting/components/modals.dart';
import 'package:consulting/main/AppNavigation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Approve extends StatefulWidget {
  final codeId;
  final bool isRegister;

  const Approve({Key key, this.codeId, this.isRegister}) : super(key: key);

  @override
  _ApproveState createState() => _ApproveState();
}

class _ApproveState extends State<Approve> {
  TextEditingController code = TextEditingController();

  sendCode() async {
    final token = (await SharedPreferences.getInstance()).get('device_token');
    print(token);
    print(this.widget.codeId);
    final response = await post(
        Uri.parse(widget.isRegister
            ? '$apiEndPoint/auth/register/check'
            : '$apiEndPoint/auth/login/check'),
        body: {
          "code": code.text,
          "code_id": this.widget.codeId.toString(),
          "device_token": token
        },
        headers: {
          "Accept": "application/json"
        });
    final body = jsonDecode(response.body);
    if (response.statusCode == 200) {
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('token', body['token']);
      pushNewScreen(context, screen: PinCode(isLogin: 0));
    } else {
      String message = '';
      if (body['errors'] != null) {
        body['errors'].forEach((k, v) => message += '\n ${v[0]}');
      } else {
        message = body['message'];
      }
      showErrorModal(context, message);
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Подтверждение',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(left: 15, right: 15),
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Телефон',
                    style: GoogleFonts.openSans(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff545454)),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 50),
                    padding: EdgeInsets.only(top: 40),
                    child: PinCodeTextField(
                      controller: code,
                      onChanged: (String value) {},
                      length: 4,
                      appContext: context,
                      cursorColor: Colors.grey,
                      pinTheme: PinTheme(
                          disabledColor: Colors.grey,
                          activeColor: Colors.grey,
                          inactiveColor: Colors.grey,
                          selectedColor: Colors.grey),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 35),
                    child: Center(
                      child: Text(
                        'Вам отправлено сообщение с 4-х значным кодом, для подтверждения вашего номера телефона',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.openSans(
                            fontSize: 12,
                            fontWeight: FontWeight.w600,
                            color: Colors.grey),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(child: Container()),
            GestureDetector(
              onTap: () => sendCode(),
              child: Container(
                margin: EdgeInsets.only(bottom: 10),
                width: width - 24,
                padding: EdgeInsets.symmetric(vertical: 15),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    border: Border.all(color: Theme.of(context).primaryColor)),
                child: Container(
                  child: Text(
                    'Продолжить',
                    style: GoogleFonts.openSans(
                        color: Theme.of(context).primaryColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
