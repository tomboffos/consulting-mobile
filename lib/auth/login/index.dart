import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:consulting/auth/client/pincode.dart';
import 'package:consulting/auth/client/register.dart';
import 'package:consulting/auth/login/approve.dart';
import 'package:consulting/components/modals.dart';
import 'package:consulting/main/AppNavigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class LoginIndex extends StatefulWidget {
  @override
  _LoginIndexState createState() => _LoginIndexState();
}

class _LoginIndexState extends State<LoginIndex> {
  TextEditingController phone = new MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');


  login()async{
    final response = await post(Uri.parse('$apiEndPoint/auth/login'),headers:{
      "Accept" : "application/json"
    },body: {
      "phone" : phone.text
    });
    final body = jsonDecode(response.body);
    print(body);
    if(response.statusCode == 200){
      pushNewScreen(context, screen: Approve(isRegister:false,codeId: body['code']['id'],));
    }else{
      String message = '';
      body['errors'].forEach((k,v)=>message += '\n ${v[0]}');
      showErrorModal(context, message);
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        automaticallyImplyLeading: false,
        title: Text(
          'Вход',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),

      ),
      body: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(left: 15, right: 15),
        child: Column(
          children: [

            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Телефон',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: TextField(
                      controller: phone,
                      style: Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: BorderSide(
                                color: Color(0xff545454),
                              )),
                          hintText: '+7 (777) 777-77-77',
                          hintStyle: Theme.of(context).textTheme.bodyText2),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(child: Container()),
            GestureDetector(
              onTap: ()=>login(),
              child: Container(
                margin: EdgeInsets.only(bottom: 10),
                width: width - 24,
                padding: EdgeInsets.symmetric(vertical: 15),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context).primaryColor
                    )
                ),
                child: Container(
                  child: Text(
                    'Продолжить',
                    style: GoogleFonts.openSans(
                        color: Theme.of(context).primaryColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );

  }
}
