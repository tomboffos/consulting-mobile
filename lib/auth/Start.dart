import 'package:consulting/auth/Register.dart';
import 'package:consulting/auth/client/register.dart';
import 'package:consulting/main/AppNavigation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'login/index.dart';

class StartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 12, right: 12, top: 150),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Text(
                'Space Learning',
                style: Theme.of(context).textTheme.headline1,
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              width: width - 24,
              padding: EdgeInsets.symmetric(horizontal: 20),
              margin: EdgeInsets.only(top: 16),
              child: Text(
                'Получите любую консультацию которую желатете или же сами станьте тем кто будет помогать консультируя',
                style: Theme.of(context).textTheme.bodyText1,
                textAlign: TextAlign.center,
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>RegisterClient()));
              },
              child: Container(
                width: width - 24,
                margin: EdgeInsets.only(top: 20),
                child: Container(
                    padding: EdgeInsets.symmetric(vertical: 14, horizontal: 12),
                    alignment: Alignment.center,
                    child: Text(
                      'Получить консультацию',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(color: Theme.of(context).primaryColor),
                    )),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Register()));
              },
              child: Container(
                width: width - 24,
                margin: EdgeInsets.only(top: 12),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 14, horizontal: 12),
                  alignment: Alignment.center,
                  child: Text(
                    'Стать консультантом',
                    style: Theme.of(context).textTheme.headline3,
                  ),
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context).secondaryHeaderColor),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>AppNavigation()));
              },
              child: Container(
                margin: EdgeInsets.only(top: 24),
                child: Text(
                  'Зарегистрироваться позже',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
            Expanded(child: Container()),
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginIndex())),
              child: Container(
                margin: EdgeInsets.only(bottom: 20),
                padding: EdgeInsets.symmetric(vertical: 15),
                width: width - 24,
                alignment: Alignment.center,
                decoration: BoxDecoration(color: Theme.of(context).primaryColor),
                child: Text(
                  'Войти',
                  style: GoogleFonts.openSans(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.w600),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
