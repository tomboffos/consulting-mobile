import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:consulting/auth/login/index.dart';
import 'package:consulting/main/AppNavigation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PinCode extends StatefulWidget {

  final isLogin;

  const PinCode({Key key, this.isLogin}) : super(key: key);

  @override
  _PinCodeState createState() => _PinCodeState();
}

class _PinCodeState extends State<PinCode> {


  String numberCode = '';



  void addNumbers(String number)async{
    if(numberCode.length <=4 ){
      setState(() {

        numberCode = numberCode+'$number';

      });
      if(numberCode.length == 4){
        final prefs = await SharedPreferences.getInstance();
        if(widget.isLogin != 1){
          prefs.setString('code', numberCode);
          print(numberCode);

          AwesomeDialog(
              context: context,
              borderSide: BorderSide(color: Colors.green, width: 2),
              width: 500,
              buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
              headerAnimationLoop: true,
              animType: AnimType.BOTTOMSLIDE,
              title: 'Успешно',
              desc: 'Ваш код успешно сохранен',
              showCloseIcon: true,
              dialogType: DialogType.SUCCES,
              btnOkOnPress: ()=>Navigator.pop(context)
          )..show();
          if(widget.isLogin == 0){
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>AppNavigation()), (route) => false);

          }
        }else{
          final pinCode = prefs.getString('code');
          print(pinCode);
          if(pinCode == numberCode)
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>AppNavigation()), (route) => false);
          else
            AwesomeDialog(
                context: context,
                borderSide: BorderSide(color: Colors.green, width: 2),
                width: 500,
                buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
                headerAnimationLoop: true,
                animType: AnimType.BOTTOMSLIDE,
                title: 'Ошибка',
                desc: 'Не правильный код',
                showCloseIcon: true,
                dialogType: DialogType.ERROR,
                btnOkOnPress: ()=>{}

            )..show();

        }
        numberCode = '';

      }
    }else{
      setState(() {
        numberCode = '';
      });
    }




  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 10),
              child: Text(
                'Pin code',
                style: GoogleFonts.openSans(
                    fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            Container(
              child: Text(
                widget.isLogin == 1 ? 'Введите пин код чтобы войти'  :  'Создайте ваш pin-code для приложения ',
                style: GoogleFonts.openSans(
                    fontSize: 14, fontWeight: FontWeight.w400),
              ),
            ),
            SizedBox(
              height: 100,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  numberCode.length == 0 ? CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 6,
                  ) : Text(numberCode[0]),
                  numberCode.length <= 1 ? CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 6,
                  ) : Text(numberCode[1]),
                  numberCode.length <= 2 ? CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 6,
                  ) : Text(numberCode[2]),
                  numberCode.length <= 3 ? CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 6,
                  ) : Text(numberCode[3]),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 80),
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Wrap(
                children: [
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => addNumbers('1'),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100),
                        side: BorderSide(
                          color: Theme.of(context).primaryColor
                        )
                      ),
                      child: Text('1',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(right: MediaQuery.of(context).size.width/7,left: MediaQuery.of(context).size.width/7,bottom: 16),
                    child: FloatingActionButton(
                      onPressed: () => addNumbers('2'),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('2',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => addNumbers('3'),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('3',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => addNumbers('4'),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('4',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(right: MediaQuery.of(context).size.width/7,left: MediaQuery.of(context).size.width/7,bottom: 16),
                    child: FloatingActionButton(
                      onPressed: () => addNumbers('5'),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('5',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => addNumbers('6'),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('6',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => addNumbers('7'),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('7',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(right: MediaQuery.of(context).size.width/7,left: MediaQuery.of(context).size.width/7,bottom: 16),
                    child: FloatingActionButton(
                      onPressed: () => addNumbers('8'),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('8',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: () => addNumbers('9'),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('9',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  SizedBox(
                    width: 55,
                  ),

                  Container(
                    margin: EdgeInsets.only(right: MediaQuery.of(context).size.width/7,left: MediaQuery.of(context).size.width/7,bottom: 16),
                    child: FloatingActionButton(
                      onPressed: () => addNumbers('0'),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Text('0',style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).primaryColor,

                      ),),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      onPressed: (){
                        setState(() {
                          numberCode = numberCode.substring(0,numberCode.length - 1);
                        });
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(
                              color: Theme.of(context).primaryColor
                          )
                      ),
                      child: Icon(Icons.backspace,color: Theme.of(context).primaryColor,),
                      backgroundColor: Colors.white,
                    ),
                  ),


                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
