import 'package:consulting/auth/login/pincode.dart';
import 'package:consulting/auth/success.dart';
import 'package:consulting/consultant/ConsultantNavigation.dart';
import 'package:consulting/main/AppNavigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        automaticallyImplyLeading: false,
        title: Text(
          'Регистрация',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        actions: [
          GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>AppNavigation()));
            },
            child: Container(
              margin: EdgeInsets.only(right: 20),
              alignment: Alignment.center,
              child: Text(
                'Пропустить',
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
          )
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(left: 15, right: 15),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 10),
              alignment: Alignment.centerLeft,
              child: Text(
                'Станьте консультантом прямо сейчас',
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Имя',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: TextField(
                      style: Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: BorderSide(
                                color: Color(0xff545454),
                              )),
                          hintText: 'Владимир',
                          hintStyle: Theme.of(context).textTheme.bodyText2),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Фамилия',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: TextField(
                      style: Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: BorderSide(
                                color: Color(0xff545454),
                              )),
                          hintText: 'Куперации',
                          hintStyle: Theme.of(context).textTheme.bodyText2),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Телефон',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: TextField(
                      style: Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: BorderSide(
                                color: Color(0xff545454),
                              )),
                          hintText: '+7 777 777 77 77',
                          hintStyle: Theme.of(context).textTheme.bodyText2),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(child: Container()),
            GestureDetector(
              onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>SuccessPage())),
              child: Container(
                margin: EdgeInsets.only(bottom: 10),
                width: width - 24,
                padding: EdgeInsets.symmetric(vertical: 15),
                alignment: Alignment.center,
                color: Theme.of(context).primaryColor,
                child: Container(
                  child: Text(
                    'Зарегистрироваться',
                    style: GoogleFonts.openSans(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>PinCodeLogin())),
              child: Container(
                margin: EdgeInsets.only(bottom: 10),
                width: width - 24,
                padding: EdgeInsets.symmetric(vertical: 15),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Theme.of(context).secondaryHeaderColor
                  )
                ),
                child: Container(
                  child: Text(
                    'Войти',
                    style: GoogleFonts.openSans(
                        color: Theme.of(context).secondaryHeaderColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
