import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CreateReview extends StatefulWidget {
  @override
  _CreateReviewState createState() => _CreateReviewState();
}

class _CreateReviewState extends State<CreateReview> {
  int rating = 0;

  setRating(int digit) {
    setState(() {
      rating = digit;
    });
  }

  saveReview(){
    
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Профиль преподователя',
          style: GoogleFonts.openSans(
              fontSize: 18,
              fontWeight: FontWeight.w700,
              color: Color(0xff191919)),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xff191919)),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 12, top: 20),
                  child: Text('Оцените человека',
                      style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        child: Icon(
                          rating <= 0 ? Icons.star_border : Icons.star,
                          size: 50,
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                        onTap: () => setRating(1),
                      ),
                      GestureDetector(
                        onTap: () => setRating(2),
                        child: Icon(
                            rating <= 1 ? Icons.star_border : Icons.star,
                            size: 50,
                            color: Theme.of(context).secondaryHeaderColor),
                      ),
                      GestureDetector(
                        onTap: () => setRating(3),
                        child: Icon(
                            rating <= 2 ? Icons.star_border : Icons.star,
                            size: 50,
                            color: Theme.of(context).secondaryHeaderColor),
                      ),
                      GestureDetector(
                        onTap: () => setRating(4),
                        child: Icon(
                            rating <= 3 ? Icons.star_border : Icons.star,
                            size: 50,
                            color: Theme.of(context).secondaryHeaderColor),
                      ),
                      GestureDetector(
                        onTap: () => setRating(5),
                        child: Icon(
                            rating <= 4 ? Icons.star_border : Icons.star,
                            size: 50,
                            color: Theme.of(context).secondaryHeaderColor),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: TextField(
                    maxLines: 10,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Напишите ваш отзыв...',
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0xff545454),
                          ),
                          borderRadius: BorderRadius.circular(0)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff545454)),
                          borderRadius: BorderRadius.circular(0)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff545454)),
                          borderRadius: BorderRadius.circular(0)),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(bottom: 16, left: 12, right: 12),
        child: RaisedButton(
          onPressed: () => {},
          color: Theme.of(context).primaryColor,
          child: Text(
            'Создать отзыв',
            style: GoogleFonts.openSans(
                fontSize: 14, fontWeight: FontWeight.w600, color: Colors.white),
          ),
          padding: EdgeInsets.symmetric(vertical: 15),
        ),
      ),
    );
  }
}
