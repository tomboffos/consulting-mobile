import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/profile/order/item.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';


class OrderHistory extends StatefulWidget {
  @override
  _OrderHistoryState createState() => _OrderHistoryState();
}

class _OrderHistoryState extends State<OrderHistory> {
  List<dynamic> orders;

  fetchOrderHistory()async{
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = jsonDecode((await get(Uri.parse('$apiEndPoint/user/consultation'),headers:{
      "Authorization" : "Bearer $token",
      "Accept" : "application/json"
    })).body);
    setState(() {
      orders = response['data'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchOrderHistory();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'История заказов',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(color: Colors.black),
      ),

      backgroundColor: Colors.white,
      body:orders == null ? Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ) : ListView.builder(itemBuilder: (context,index)=>OrderItem(order:orders[index]),itemCount: orders.length,),
    );
  }
}
