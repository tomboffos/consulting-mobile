import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class OrderItem extends StatelessWidget {
  final order;

  const OrderItem({Key key, this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 0, ),
        leading: CircleAvatar(
          backgroundColor: Colors.grey,
          foregroundImage: order['profile']['user']['avatar'] == null ? NetworkImage('https://personal-lawyer.a-lux.dev/storage/users/default.png')
              : NetworkImage(order['profile']['user']['avatar']),

        ),
        title: Text(
          order['profile']['user']['name'],
          style: GoogleFonts.openSans(
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: Color(0xff040404)),
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                RichText(
                    text: TextSpan(
                        text: 'Статус заказа:',
                        style: GoogleFonts.openSans(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                        ),
                        children: [
                          TextSpan(
                              text: order['consultation_status']['name'],
                              style:
                              GoogleFonts.openSans(fontWeight: FontWeight.w400))
                        ])),
                SizedBox(
                  width: 10,
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 3),
              child: RichText(
                  text: TextSpan(
                      text: 'Ваш предмет: ',
                      style: GoogleFonts.openSans(
                          fontSize: 13,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                      children: [
                        TextSpan(
                            text: order['profile']['category']['name'],
                            style: GoogleFonts.openSans(
                              fontWeight: FontWeight.w400,
                            ))
                      ])),
            )
          ],
        ),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Text(
                '${order['price']} ${order['profile']['currency']['symbol']}',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff191919)),
              ),
            ),
            Container(
              child: Text(order['created_at'],style: GoogleFonts.openSans(
                fontSize: 13,
                fontWeight: FontWeight.w400
              ),),
            )
          ],
        ),
        isThreeLine: true,
      ),
    );
  }
}
