import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/payments/link_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:slide_popup_dialog/slide_popup_dialog.dart';

class CardIndex extends StatefulWidget {
  @override
  _CardIndexState createState() => _CardIndexState();
}

class _CardIndexState extends State<CardIndex> {
  List<dynamic> cards = [];

  bool loading = false;

  linkCard() async {
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await post(Uri.parse('$apiEndPoint/user/cards'), headers: {
      "Authorization": "Bearer $token",
      "Accept": "application/json"
    });
    final body = jsonDecode(response.body);
    print(body);
    if (response.statusCode == 200) {
      setState(() {
        loading = false;
      });
      pushNewScreen(context,
          screen: LinkCardWebView(
            link: body['xml']['pg_redirect_url'],
          ));
    }
  }

  fetchCards() async {
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await get(Uri.parse('$apiEndPoint/user/cards'), headers: {
      "Authorization": "Bearer $token",
      "Accept": "application/json"
    });
    final body = jsonDecode(response.body);
    print(body);
    if (response.statusCode == 200) {
      setState(() {
        loading = false;
        cards = body['data'];
      });
    }
  }

  void showCardSettings(card) {
    showSlideDialog(
      context: context,
      child: Container(
        child: Column(
          children: [
            GestureDetector(
              onTap: ()=>isMainCard(card),
              child: Container(
                width: MediaQuery.of(context).size.width,

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(

                      margin: EdgeInsets.symmetric(vertical: 20,horizontal: 12),
                      child: Text('Основная карта',style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,

                      ),),
                    ),
                    Divider(),

                  ],
                ),
              ),
            ),
            GestureDetector(

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,

                    margin: EdgeInsets.symmetric(vertical: 20,horizontal: 12),
                    child: Text('Удалить',style: GoogleFonts.openSans(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.red

                    ),),
                  ),
                  Divider(),

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    fetchCards();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Мои карты',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      backgroundColor: Colors.white,
      body: loading
          ? Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : SingleChildScrollView(
              physics: ScrollPhysics(),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                child: Column(
                  children: [
                    ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (context, index) => Container(
                        child: GestureDetector(
                          onTap: () => showCardSettings(cards[index]),
                          child: Container(
                            margin: EdgeInsets.symmetric(vertical: 8),
                            decoration: BoxDecoration(boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                spreadRadius: 2,
                                blurRadius: 4,
                              ),
                            ], color: Colors.white),
                            child: ListTile(
                              leading:
                                  Image.asset('assets/images/mastercard.jpg'),
                              title: Text(
                                '${cards[index]['card_hash']}',
                                style: GoogleFonts.openSans(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff191919)),
                              ),
                              trailing: Container(
                                width: 70,
                                child: Row(
                                  children: [
                                    cards[index]['is_main'] == true
                                        ? Container(
                                            margin: EdgeInsets.only(right: 22),
                                            child: Icon(
                                              Icons.check_circle,
                                              color: Theme.of(context)
                                                  .secondaryHeaderColor,
                                            ),
                                          )
                                        : SizedBox.shrink(),
                                    Spacer(),
                                    Icon(
                                      Icons.chevron_right,
                                      color: Colors.black,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      itemCount: cards.length,
                    ),
                    GestureDetector(
                      onTap: () => linkCard(),
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 8),
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            spreadRadius: 2,
                            blurRadius: 7,
                          ),
                        ], color: Colors.white),
                        padding: EdgeInsets.only(
                            top: 16, bottom: 16, left: 20, right: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Добавить карту',
                              style: GoogleFonts.openSans(
                                  fontSize: 14, fontWeight: FontWeight.w400),
                            ),
                            Icon(Icons.chevron_right)
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }

  isMainCard(card) async{
    final token = (await SharedPreferences.getInstance()).get('token');
    print(card);
    final response = await post(Uri.parse('$apiEndPoint/user/card/main/${card['id']}'),headers:{
      "Authorization" : "Bearer $token",
      "Accept" : "application/json"
    });

    print(response.body);
    if(response.statusCode == 200){
      await fetchCards();
    }


  }
}
