import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/components/modals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileEdit extends StatefulWidget {
  final Map<String,dynamic> user;

  const ProfileEdit({Key key, this.user}) : super(key: key);
  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  TextEditingController name = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController surname = new TextEditingController();

  Map<String,dynamic> user;
  @override
  void initState() {
    // TODO: implement initState
    user = widget.user;
    name.text = user['name'];
    email.text = user['email'];
    surname.text = user['surname'];
    super.initState();
  }

  void updateProfile()async{
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');

    final response = await post(Uri.parse('$apiEndPoint/user/update'),headers: {
      "Authorization" : "Bearer $token"
    },body: {
      "name" : name.text,
      "surname" : surname.text,
      "email" : email.text
    });
    final body = jsonDecode(response.body);
    print(body);
    if(response.statusCode == 200){
      showSuccessModel(context, 'Ваш профиль успешно обновлен');
    }else{
      String message = '';
      body['errors'].forEach((k,v)=>message += '\n ${v[0]}');
      showErrorModal(context, message);
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Личные данные',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 16),
                padding: EdgeInsets.all(7),
                decoration: BoxDecoration(
                    border: Border.all(color: Theme.of(context).primaryColor),
                    borderRadius: BorderRadius.circular(100)),
                child: CircleAvatar(
                  foregroundImage: NetworkImage(user['avatar'] != null ? user['avatar'] : 'https://personal-lawyer.a-lux.dev/storage/users/default.png'),
                  radius: 34,
                  backgroundColor: Colors.grey,
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Имя',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: TextField(
                        controller: name,
                        style: Theme.of(context).textTheme.bodyText2,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0),
                                borderSide: BorderSide(
                                  color: Color(0xff545454),
                                )),
                            hintText: 'Владимир',
                            hintStyle: Theme.of(context).textTheme.bodyText2),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Фамилия',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: TextField(
                        controller: surname,
                        style: Theme.of(context).textTheme.bodyText2,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0),
                                borderSide: BorderSide(
                                  color: Color(0xff545454),
                                )),
                            hintText: 'Владимир',
                            hintStyle: Theme.of(context).textTheme.bodyText2),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Почта',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: TextField(
                        controller: email,
                        style: Theme.of(context).textTheme.bodyText2,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0),
                                borderSide: BorderSide(
                                  color: Color(0xff545454),
                                )),
                            hintText: '',
                            hintStyle: Theme.of(context).textTheme.bodyText2),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: RaisedButton(
        onPressed: () => updateProfile(),
        color: Theme.of(context).primaryColor,
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Text('Сохранить',style: GoogleFonts.openSans(
            fontSize: 14,
            fontWeight: FontWeight.w600,
            color: Colors.white
          ),),
        ),
      ),
    );
  }
}
