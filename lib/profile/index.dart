import 'dart:convert';
import 'dart:ui';

import 'package:consulting/auth/Start.dart';
import 'package:consulting/auth/client/pincode.dart';
import 'package:consulting/auth/client/register.dart';
import 'package:consulting/consultant/ConsultantNavigation.dart';
import 'package:consulting/main/AppNavigation.dart';
import 'package:consulting/profile/card/index.dart';
import 'package:consulting/profile/edit.dart';
import 'package:consulting/profile/order/history.dart';
import 'package:consulting/profile/support/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileIndex extends StatefulWidget {
  @override
  _ProfileIndexState createState() => _ProfileIndexState();
}

class _ProfileIndexState extends State<ProfileIndex> {
  Map<String, dynamic> user;

  checkToken() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    if (token != null) {
      final response = await get(Uri.parse('$apiEndPoint/user'),
          headers: {"Authorization": "Bearer $token"});
      final body = jsonDecode(response.body);
      print(body);
      setState(() {
        user = body['data'];
      });
    }
  }

  logout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    pushNewScreen(context,
        screen: AppNavigation(), withNavBar: false);
  }

  @override
  void initState() {
    // TODO: implement initState
    checkToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: user == null
          ? Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            child: Text(
              'Зарегистрируйтесь или войдите',
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            width: MediaQuery
                .of(context)
                .size
                .width - 20,
            padding: EdgeInsets.symmetric(horizontal: 47),
            margin: EdgeInsets.only(bottom: 18, top: 20),
            child: RaisedButton(
              onPressed: () =>
                  pushNewScreen(context,
                      screen: StartScreen(), withNavBar: false),
              color: Theme
                  .of(context)
                  .primaryColorDark,
              child: Text(
                'Войти',
                style: GoogleFonts.roboto(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Colors.white),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100)),
              padding: EdgeInsets.symmetric(vertical: 16),
            ),
          )
        ],
      )
          : Container(
        margin: EdgeInsets.only(top: 50),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 23),
              margin: EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      right: 10,
                    ),
                    padding: EdgeInsets.all(6),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Theme
                                .of(context)
                                .primaryColor),
                        borderRadius: BorderRadius.circular(100)),
                    child: CircleAvatar(
                      foregroundImage: NetworkImage(user['avatar'] != null
                          ? user['avatar']
                          : 'https://personal-lawyer.a-lux.dev/storage/users/default.png'),
                      backgroundColor: Colors.grey,
                      radius: 27,
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 5),
                        child: Text(
                          '${user['name']} ${user['surname']}',
                          style: GoogleFonts.openSans(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                      ),
                      GestureDetector(
                        onTap: () =>
                            pushNewScreen(context,
                                screen: ProfileEdit(user: user,),
                                withNavBar: false),
                        child: Container(
                          child: Text(
                            'Редактировать профиль',
                            style: GoogleFonts.openSans(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.grey),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            user['profiles'].length > -1 ? GestureDetector(
              onTap: () =>
                  pushNewScreen(context,
                      screen: ConsultantNavigation(), withNavBar: false),
              child: Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                padding: EdgeInsets.only(bottom: 13),
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: Text(
                  'Переключиться в режим преподавателя',
                  style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Theme
                          .of(context)
                          .primaryColor),
                ),
              ),
            ) : SizedBox.shrink(),
            Divider(
              color: Colors.grey,
            ),
            GestureDetector(
              onTap: () =>
                  pushNewScreen(context,
                      screen: PinCode(), withNavBar: false),
              child: Container(
                padding:
                EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  child: Text(
                    'Сменить код доступа',
                    style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)),
                  ),
                ),
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            GestureDetector(
              onTap: () =>
                  pushNewScreen(context,
                      screen: OrderHistory(), withNavBar: false),
              child: Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                padding:
                EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Container(
                  child: Text(
                    'История заказов',
                    style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)),
                  ),
                ),
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            GestureDetector(
              onTap: () =>
                  pushNewScreen(context,
                      screen: CardIndex(), withNavBar: false),
              child: Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                padding:
                EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Container(
                  child: Text(
                    'Мои карты',
                    style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)),
                  ),
                ),
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            GestureDetector(
              onTap: () =>
                  pushNewScreen(context,
                      screen: SupportIndex(), withNavBar: false),
              child: Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                padding:
                EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Container(
                  child: Text(
                    'Служба поддержки',
                    style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)),
                  ),
                ),
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            GestureDetector(
              onTap: () => logout(),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  child: Text(
                    'Выйти',
                    style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
