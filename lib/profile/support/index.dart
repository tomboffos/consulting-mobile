import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SupportIndex extends StatefulWidget {
  @override
  _SupportIndexState createState() => _SupportIndexState();
}

class _SupportIndexState extends State<SupportIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Служба поддержки',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 16),
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Телефон',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: TextField(
                      style: Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: BorderSide(
                                color: Color(0xff545454),
                              )),
                          hintText: '87073039191',
                          hintStyle: Theme.of(context).textTheme.bodyText2),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(
                top: 18,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: TextField(
                      style: Theme.of(context).textTheme.bodyText2,
                      maxLines: 8,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: BorderSide(
                                color: Color(0xff545454),
                              )),
                          hintText: 'Что вас беспокоит?',
                          hintStyle: Theme.of(context).textTheme.bodyText2),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 130,
            ),
            Container(
              width: MediaQuery.of(context).size.width-32,
              height: 50,
              child: RaisedButton(
                onPressed: () => {},
                color: Theme.of(context).primaryColor,
                child: Text('Отправить',style: GoogleFonts.openSans(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: Colors.white
                ),),
              ),
            )
          ],
        ),
      ),
    );
  }
}
