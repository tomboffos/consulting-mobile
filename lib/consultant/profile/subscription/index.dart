import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/consultant/profile/subscription/item.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MySubscriptions extends StatefulWidget {
  @override
  _MySubscriptionsState createState() => _MySubscriptionsState();
}

class _MySubscriptionsState extends State<MySubscriptions> {
  List<dynamic> subscriptions;

  fetchSubscriptions() async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(
        Uri.parse('$apiEndPoint/consultant/subscriptions'),
        headers: {"Authorization": "Bearer $token"});

    setState(() {
      subscriptions = jsonDecode(response.body)['data'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchSubscriptions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Мои подписки',
          style: GoogleFonts.openSans(
              fontSize: 18,
              fontWeight: FontWeight.w700,
              color: Color(0xff191919)),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xff191919)),
        elevation: 0,
      ),
      body: subscriptions == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : RefreshIndicator(
              onRefresh: () => fetchSubscriptions(),
              child: ListView.builder(
                itemBuilder: (context, index) => MySubscriptionItem(
                  subscription: subscriptions[index],
                ),
                itemCount: subscriptions.length,
              ),
            ),
    );
  }
}
