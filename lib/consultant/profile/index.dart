import 'package:consulting/auth/Start.dart';
import 'package:consulting/auth/client/pincode.dart';
import 'package:consulting/consultant/form/index.dart';
import 'package:consulting/consultant/profile/subscription/index.dart';
import 'package:consulting/main/AppNavigation.dart';
import 'package:consulting/profile/card/index.dart';
import 'package:consulting/profile/edit.dart';
import 'package:consulting/profile/order/history.dart';
import 'package:consulting/profile/support/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class ConsultantProfileIndex extends StatefulWidget {
  @override
  _ConsultantProfileIndexState createState() => _ConsultantProfileIndexState();
}

class _ConsultantProfileIndexState extends State<ConsultantProfileIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 23),
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 10,),
                      padding:EdgeInsets.all(6),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Theme.of(context).secondaryHeaderColor
                          ),
                          borderRadius: BorderRadius.circular(100)
                      ),
                      child: CircleAvatar(
                        backgroundColor: Colors.grey,
                        radius: 27,
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 5),
                          child: Text('Мурат Арканович',style: GoogleFonts.openSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w600
                          ),),
                        ),
                        GestureDetector(
                          onTap: ()=>pushNewScreen(context, screen: ProfileEdit(),withNavBar: false),

                          child: Container(
                            child: Text('Редактировать профиль',style: GoogleFonts.openSans(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.grey
                            ),),
                          ),
                        )
                      ],
                    )

                  ],
                ),
              ),
              GestureDetector(
                onTap: ()=>pushNewScreen(context, screen: AppNavigation(),withNavBar: false),
                child: Container(
                  width: MediaQuery.of(context).size.width,

                  padding: EdgeInsets.only(bottom: 13),
                  margin: EdgeInsets.symmetric(horizontal: 15),
                  child: Text('Переключиться в режим ученика',style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Theme.of(context).secondaryHeaderColor
                  ),),
                ),
              ),
              Divider(color: Colors.grey,),
              GestureDetector(
                onTap: ()=>pushNewScreen(context, screen: MySubscriptions(),withNavBar: false),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
                  child: Container(
                    child: Text('Мои подписки',style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)
                    ),),

                  ),
                ),
              ),
              Divider(color: Colors.grey,),

              GestureDetector(
                onTap: ()=>pushNewScreen(context, screen: IndexForm(),withNavBar: false),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
                  child: Container(
                    child: Text('Мои анкеты',style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)
                    ),),

                  ),
                ),
              ),
              Divider(color: Colors.grey,),
              GestureDetector(
                onTap: ()=>pushNewScreen(context, screen: CardIndex(),withNavBar: false),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
                  child: Container(
                    child: Text('Мои карты',style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)
                    ),),

                  ),
                ),
              ),
              Divider(color: Colors.grey,),

              GestureDetector(
                onTap: ()=>pushNewScreen(context, screen: PinCode(),withNavBar: false),
                child: Container(
                  width: MediaQuery.of(context).size.width,

                  padding: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
                  child: Container(
                    width: MediaQuery.of(context).size.width,

                    child: Text('Сменить код доступа',style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)
                    ),),

                  ),
                ),
              ),
              Divider(color: Colors.grey,),

              GestureDetector(
                onTap: ()=>pushNewScreen(context, screen: SupportIndex(),withNavBar: false),
                child: Container(
                  width: MediaQuery.of(context).size.width,

                  padding: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
                  child: Container(
                    child: Text('Служба поддержки',style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)
                    ),),

                  ),
                ),
              ),
              Divider(color: Colors.grey,),
              GestureDetector(

                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
                  child: Container(
                    width: MediaQuery.of(context).size.width,

                    child: Text('Выйти',style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff191919)
                    ),),

                  ),
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }
}
