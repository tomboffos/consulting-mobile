import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/catalog/Catalog.dart';
import 'package:consulting/chat/index.dart';
import 'package:consulting/consultant/application/index.dart';
import 'package:consulting/consultant/profile/index.dart';
import 'package:consulting/consultant/subscription/index.dart';
import 'package:consulting/main/MainPage.dart';
import 'package:consulting/profile/index.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConsultantNavigation extends StatefulWidget {
  @override
  _ConsultantNavigationState createState() => _ConsultantNavigationState();
}

class _ConsultantNavigationState extends State<ConsultantNavigation> {
  List<dynamic> orders;

  List<Widget> _buildScreens() {
    return [
      SubscriptionIndex(),
      ApplicationIndex(
        orders: orders,
      ),
      ChatIndex(
        isConsultant: true,
      ),
      ConsultantProfileIndex(),
    ];
  }

  PersistentTabController _controller;

  void fetchOrders() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(Uri.parse('$apiEndPoint/consultant/orders'),
        headers: {"Authorization": "Bearer $token"});
    print(response.body);
    if (response.statusCode == 200)
      setState(() {
        orders = jsonDecode(response.body)['data'];
      });
  }

  @override
  void initState() {
    fetchOrders();
    // TODO: implement initState
    super.initState();

    _controller = PersistentTabController(initialIndex: 0);
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
          icon: Icon(Icons.dashboard),
          title: "Главная",
          activeColorPrimary: Theme.of(context).secondaryHeaderColor,
          inactiveColorPrimary: Colors.grey),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.class_),
          title: ("Мои заявки"),
          activeColorPrimary: Theme.of(context).secondaryHeaderColor,
          inactiveColorPrimary: Colors.grey),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.message),
          title: ("Чат"),
          activeColorPrimary: Theme.of(context).secondaryHeaderColor,
          inactiveColorPrimary: Colors.grey),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.person),
        title: ("Профиль"),
        activeColorPrimary: Theme.of(context).secondaryHeaderColor,
        inactiveColorPrimary: Colors.grey,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(),
      items: _navBarsItems(),
      backgroundColor: Colors.white,
      handleAndroidBackButtonPress: true,
      resizeToAvoidBottomInset: true,
      onItemSelected: (int) {
        fetchOrders();
        setState(
            () {}); // This is required to update the nav bar if Android back button is pressed
      },
      stateManagement: true,
      hideNavigationBarWhenKeyboardShows: true,
      decoration: NavBarDecoration(borderRadius: BorderRadius.circular(0)),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      navBarStyle: NavBarStyle.style6,
    );
  }
}
