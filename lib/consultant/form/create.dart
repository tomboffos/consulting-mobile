import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/components/modals.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateForm extends StatefulWidget {
  @override
  _CreateFormState createState() => _CreateFormState();
}

class _CreateFormState extends State<CreateForm> {
  String category_id = "1";
  List<dynamic> categories;
  TextEditingController description = new TextEditingController();
  TextEditingController price = new TextEditingController();
  DateTime startDate = DateTime.now();
  void fetchCategories() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = jsonDecode((await get(
            Uri.parse('$apiEndPoint/consultant/categories'),
            headers: {"Authorization": "Bearer $token"}))
        .body)['data'];
    setState(() {
      categories = response;
    });
  }

  void storeProfileRequest() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response =
        await post(Uri.parse('$apiEndPoint/consultant/profile'), body: {
      "description": description.text,
      "start_date": startDate.toString(),
      "price": price.text,
      "category_id": category_id
    }, headers: {
      "Authorization": "Bearer $token", "Accept" : "application/json"
    });
    String message = '';
    final body = jsonDecode(response.body);
    if(response.statusCode == 422){
      body['errors'].forEach((k,v)=>message += '\n ${v[0]}');
      showErrorModal(context, message);
    }else showSuccessModel(context, 'Ваша анкета успешно создана');
  }

  showChooseDate() {
    print('he');
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime.utc(1970),
      maxTime: DateTime.now(),
      theme: DatePickerTheme(
          itemStyle: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w400, fontSize: 18),
          doneStyle: TextStyle(color: Colors.black, fontSize: 16)),
      onConfirm: (date) {
        setState(() {
          startDate = date;
        });
      },
      currentTime: startDate,
      locale: LocaleType.ru,
    );
  }

  @override
  void initState() {
    fetchCategories();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Создание анкеты',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      backgroundColor: Colors.white,
      body: categories == null
          ? Center(child: CircularProgressIndicator())
          : GestureDetector(
              onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5),
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(top: 18),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Создайте свою первую анкету',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              width: MediaQuery.of(context).size.width - 20,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 5),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                color: Colors.grey,
                              )),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  value: category_id,
                                  icon: const Icon(Icons.arrow_downward),
                                  iconSize: 24,
                                  elevation: 16,
                                  style: Theme.of(context).textTheme.bodyText2,
                                  underline: Container(
                                    height: 2,
                                    color: Colors.grey,
                                  ),
                                  onChanged: (String newValue) {
                                    setState(() {
                                      category_id = newValue;
                                    });
                                  },
                                  hint: Text(
                                    'Выбирите свою профессию',
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                  items: categories
                                      .map<DropdownMenuItem<String>>((value) {
                                    return DropdownMenuItem<String>(
                                      value: value['id'].toString(),
                                      child: Text(value['name']),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(top: 18),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Укажите ваш стаж',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            GestureDetector(
                              onTap: () => showChooseDate(),
                              child: Container(
                                margin: EdgeInsets.only(top: 5),
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 8),
                                  width: MediaQuery.of(context).size.width - 25,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey)),
                                  child: Text(
                                      '${startDate.day}.${startDate.month}.${startDate.year}'),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(
                          top: 18,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              child: TextField(
                                controller: description,
                                style: Theme.of(context).textTheme.bodyText2,
                                maxLines: 5,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(0),
                                        borderSide: BorderSide(
                                          color: Color(0xff545454),
                                        )),
                                    hintText: 'Описание вашей профессии...',
                                    hintStyle:
                                        Theme.of(context).textTheme.bodyText2),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(top: 18),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Цена за консультацию',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              child: TextField(
                                controller: price,
                                keyboardType: TextInputType.number,
                                style: Theme.of(context).textTheme.bodyText2,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(0),
                                        borderSide: BorderSide(
                                          color: Color(0xff545454),
                                        )),
                                    hintText: '3200 тг.',
                                    hintStyle:
                                        Theme.of(context).textTheme.bodyText2),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 4,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width - 32,
                        height: 50,
                        child: RaisedButton(
                          onPressed: () => storeProfileRequest(),
                          color: Theme.of(context).secondaryHeaderColor,
                          child: Text(
                            'Создать анкету',
                            style: GoogleFonts.openSans(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
