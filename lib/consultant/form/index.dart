import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/consultant/form/create.dart';
import 'package:consulting/consultant/form/show.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IndexForm extends StatefulWidget {
  @override
  _IndexFormState createState() => _IndexFormState();
}

class _IndexFormState extends State<IndexForm> {
  List<dynamic> profiles;

  fetchProfiles()async{
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(Uri.parse('$apiEndPoint/consultant/profile'),headers: {
      "Authorization" : "Bearer $token"
    });
    if(response.statusCode == 200)
      setState(() {
        profiles = jsonDecode(response.body)['data'];
      });
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchProfiles();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Мои анкеты',
          style: GoogleFonts.openSans(
              fontSize: 18,
              fontWeight: FontWeight.w700,
              color: Color(0xff191919)),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xff191919)),
        elevation: 1,
      ),
      backgroundColor: Colors.white,
      body:profiles != null ?  RefreshIndicator(
        onRefresh: ()=>fetchProfiles(),
        child: ListView.builder(
          itemBuilder: (context, index) => FormItem(profile:profiles[index]),
          itemCount: profiles.length,
        ),
      ) : Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => pushNewScreen(context, screen: CreateForm(),withNavBar: false),
        backgroundColor: Color(0xff1D9CDD),
        child: Icon(Icons.add,color: Colors.white,),
      ),
    );
  }
}
