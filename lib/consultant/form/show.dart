import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/components/modals.dart';
import 'package:consulting/consultant/form/create.dart';
import 'package:consulting/payments/payment_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormItem extends StatefulWidget {
  final Map<String,dynamic> profile;

  const FormItem({Key key, this.profile}) : super(key: key);

  @override
  _FormItemState createState() => _FormItemState();
}

class _FormItemState extends State<FormItem> {
  void storeProfilePayment()async{
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await post(Uri.parse('$apiEndPoint/consultant/profile-payments'),body: {
      "profile_id" : widget.profile['id'].toString()
    },headers: {
      "Authorization" : "Bearer $token",
      "Accept" : "application/json"
    });

    String message = '';
    if(response.statusCode == 422){
      final body = jsonDecode(response.body);
      message = body.forEach((k,v)=> message += '\n ${v['0']}');
      showErrorModal(context, message);
      return null;

    }

    if(response.statusCode == 400){
      final body = jsonDecode(response.body);
      showErrorModal(context, body['message']);
      return null;
    }

    pushNewScreen(context, screen: PaymentPage(data: response.body,));
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.only(top: 20),
      margin: EdgeInsets.symmetric(horizontal: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                children: [
                  CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 3,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  RichText(
                      text: TextSpan(
                          text: 'Профессия: ',
                          style: GoogleFonts.openSans(
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                              color: Colors.black),
                          children: [
                            TextSpan(
                                text: '${widget.profile['category']['name']}',
                                style: GoogleFonts.openSans(
                                    fontWeight: FontWeight.w400))
                          ])),
                ],
              ),
              SizedBox(
                width: 18,
              ),
              Row(
                children: [
                  CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 3,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  RichText(
                      text: TextSpan(
                          text: 'Начал работу с: ',
                          style: GoogleFonts.openSans(
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                              color: Colors.black),
                          children: [
                            TextSpan(
                                text: '${widget.profile['start_date']}',
                                style: GoogleFonts.openSans(
                                    fontWeight: FontWeight.w400))
                          ])),
                ],
              ),
              Expanded(child: Container()),
              GestureDetector(child: Icon(Icons.create,color: Colors.grey,),onTap: ()=>pushNewScreen(context, screen: CreateForm()),)
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 4),
            child: Text(
              'Описание',
              style: GoogleFonts.openSans(
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff1F1F1F)),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 4, bottom: 14),
            child: Text(
              '${widget.profile['description']}',
              style: GoogleFonts.openSans(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff434343)),
            ),
          ),
          //price
          //button
          Container(
            width: width - 32,
            margin: EdgeInsets.only(
              top: 12,
              bottom: 9,
            ),
            child: RaisedButton(
              onPressed: ()=>widget.profile['status']['id'] == 1 ? null : storeProfilePayment(),
              color: Theme.of(context).secondaryHeaderColor,
              child: Text(
                widget.profile['status']['id'] == 1 ? 'Подписка оформлена' :  'Оформить подписку',
                style: GoogleFonts.openSans(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
              padding: EdgeInsets.symmetric(vertical: 15),
            ),
          ),
          Divider(
            color: Color(0xff4E4E4E),
          )
        ],
      ),
    );
  }
}
