import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/components/modals.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApplicationItem extends StatefulWidget {
  final offer;
  final acceptFunction;
  final declineFunction;
  const ApplicationItem(
      {Key key, this.offer, this.acceptFunction, this.declineFunction})
      : super(key: key);

  @override
  _ApplicationItemState createState() => _ApplicationItemState();
}

class _ApplicationItemState extends State<ApplicationItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      child: Column(
        children: [
          ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
            leading: CircleAvatar(
              backgroundColor: Colors.grey,
              foregroundImage:
                  NetworkImage('${widget.offer['user']['avatar']}'),
            ),
            title: Text(
              '${widget.offer['user']['name']}',
              style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff040404)),
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    RichText(
                        text: TextSpan(
                            text: 'Время проведения:',
                            style: GoogleFonts.openSans(
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                            ),
                            children: [
                          TextSpan(
                              text:
                                  '${DateTime.parse(widget.offer['start_time']).hour}:${DateTime.parse(widget.offer['start_time']).minute} - ${DateTime.parse(widget.offer['end_time']).hour}:${DateTime.parse(widget.offer['end_time']).minute}',
                              style: GoogleFonts.openSans(
                                  fontWeight: FontWeight.w400))
                        ])),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 3),
                  child: RichText(
                      text: TextSpan(
                          text: 'Предмет: ',
                          style: GoogleFonts.openSans(
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                              color: Colors.black),
                          children: [
                        TextSpan(
                            text:
                                '${widget.offer['profile']['category']['name']}',
                            style: GoogleFonts.openSans(
                              fontWeight: FontWeight.w400,
                            ))
                      ])),
                )
              ],
            ),
            trailing: Text(
              '${widget.offer['created_at']}',
              style: GoogleFonts.openSans(
                  fontSize: 10,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff404040)),
            ),
            isThreeLine: true,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                  width: MediaQuery.of(context).size.width / 2 - 24,
                  child: RaisedButton(
                    onPressed: widget.declineFunction,
                    child: Text(
                      'Отклонить',
                      style: GoogleFonts.openSans(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Colors.red),
                    ),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.red)),
                  )),
              Container(
                  width: MediaQuery.of(context).size.width / 2 - 24,
                  child: RaisedButton(
                    onPressed: widget.acceptFunction,
                    child: Text(
                      'Принять',
                      style: GoogleFonts.openSans(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                    color: Theme.of(context).secondaryHeaderColor,
                  )),
            ],
          )
        ],
      ),
    );
  }
}
