import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/components/modals.dart';
import 'package:consulting/consultant/application/item.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApplicationIndex extends StatefulWidget {
  final List<dynamic> orders;

  const ApplicationIndex({Key key, this.orders}) : super(key: key);

  @override
  _ApplicationIndexState createState() => _ApplicationIndexState();
}

class _ApplicationIndexState extends State<ApplicationIndex> {
  List<dynamic> orders;
  fetchOrders() async {
    setState(() {
      orders = null;
    });
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(Uri.parse('$apiEndPoint/consultant/orders'),
        headers: {"Authorization": "Bearer $token"});
    print(response.body);
    if (response.statusCode == 200)
      setState(() {
        orders = jsonDecode(response.body)['data'];
      });
  }

  acceptOffer(offer) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await post(
        Uri.parse('$apiEndPoint/consultant/consultation/accept/${offer['id']}'),
        headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 401) {
      showErrorModal(context, jsonDecode(response.body)['message']);
    } else {
      showSuccessModel(context, 'Вы успешно приняли консультацию');
      await fetchOrders();
    }
  }

  declineOffer(offer) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await post(
        Uri.parse(
            '$apiEndPoint/consultant/consultation/decline/${offer['id']}'),
        headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 401) {
      showErrorModal(context, jsonDecode(response.body)['message']);
    } else {
      showSuccessModel(context, 'Вы успешно отклонили консультацию');
      await fetchOrders();
    }
  }

  @override
  void initState() {
    setState(() {
      orders = widget.orders;
    });

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Мои заявки',
          style: GoogleFonts.openSans(
              fontSize: 18,
              fontWeight: FontWeight.w700,
              color: Color(0xff191919)),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xff191919)),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: orders == null
          ? Center(child: CircularProgressIndicator())
          : orders.length == 0
              ? Container(
                  child: Center(
                    child: Text('Пока что нет заявок'),
                  ),
                )
              : RefreshIndicator(
                  onRefresh: () => fetchOrders(),
                  child: ListView.builder(
                    itemBuilder: (context, index) => ApplicationItem(
                        offer: orders[index],
                        acceptFunction: () => acceptOffer(orders[index]),
                        declineFunction: () => declineOffer(orders[index])),
                    itemCount: orders.length,
                  ),
                ),
    );
  }
}
