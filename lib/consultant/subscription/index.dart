import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/consultant/subscription/item.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SubscriptionIndex extends StatefulWidget {
  @override
  _SubscriptionIndexState createState() => _SubscriptionIndexState();
}

class _SubscriptionIndexState extends State<SubscriptionIndex> {
  List<dynamic> subscriptions;
  fetchSubscriptions() async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(
        Uri.parse('$apiEndPoint/consultant/subscriptions'),
        headers: {"Authorization": "Bearer $token"});

    setState(() {
      subscriptions = jsonDecode(response.body)['data'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchSubscriptions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Мои подписки',
          style: GoogleFonts.openSans(
              fontSize: 18,
              fontWeight: FontWeight.w700,
              color: Color(0xff191919)),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xff191919)),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 12),
              child: TextField(
                style: GoogleFonts.roboto(
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: Color(0xff146C99)),
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                    color: Theme.of(context).secondaryHeaderColor,
                  ),
                  hintText: 'Что вас интересует?',
                  hintStyle: GoogleFonts.roboto(
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                      color: Theme.of(context).secondaryHeaderColor),
                  enabledBorder: OutlineInputBorder(
                      gapPadding: 2,
                      borderRadius: BorderRadius.circular(0),
                      borderSide: new BorderSide(
                          color: Theme.of(context).secondaryHeaderColor,
                          width: 1.0)),
                  focusedBorder: OutlineInputBorder(
                      gapPadding: 2,
                      borderRadius: BorderRadius.circular(0),
                      borderSide: new BorderSide(
                          color: Theme.of(context).secondaryHeaderColor,
                          width: 1.0)),
                ),
              ),
            ),
            subscriptions == null ? Container(
              height: MediaQuery.of(context).size.height/2,
              width: MediaQuery.of(context).size.height,
              child: Center(child: CircularProgressIndicator()),
            ) : ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) => Subscription(subscription: subscriptions[index],),
              itemCount: subscriptions.length,
            )
          ],
        ),
      ),
    );
  }
}
