import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Subscription extends StatefulWidget {
  final Map<String,dynamic> subscription;

  const Subscription({Key key, this.subscription}) : super(key: key);

  @override
  _SubscriptionState createState() => _SubscriptionState();
}

class _SubscriptionState extends State<Subscription> {
  bool showApp = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16, horizontal: 12),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            offset: Offset(1, 0),
            spreadRadius: 1,
            blurRadius: 9,
            color: Colors.black.withOpacity(0.2))
      ]),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '${widget.subscription['category']['name']}',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Colors.black),
              ),
              GestureDetector(
                  onTap: () {
                    setState(() {
                      showApp = !showApp;
                    });
                  }, child: Icon(showApp ?  Icons.keyboard_arrow_up : Icons.keyboard_arrow_down)),
            ],
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Icon(
                  Icons.assignment,
                  color: Theme.of(context).secondaryHeaderColor,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Анкета присутствует',
                  style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff525252)),
                ),
              ],
            ),
          ),
          // showApp ? Container(
          //   child: Column(
          //     children: [
          //       Container(
          //         margin: EdgeInsets.only(bottom: 10),
          //         child: Row(
          //           children: [
          //             CircleAvatar(
          //               backgroundColor: Theme.of(context).secondaryHeaderColor,
          //               radius: 4,
          //             ),
          //             SizedBox(
          //               width: 10,
          //             ),
          //             Text('Астрофизика',style: GoogleFonts.openSans(
          //               fontSize: 14,
          //               fontWeight: FontWeight.w400,
          //               color: Color(0xff595959)
          //             ),)
          //           ],
          //         ),
          //       ),
          //       Container(
          //         margin: EdgeInsets.only(bottom: 10),
          //         child: Row(
          //           children: [
          //             CircleAvatar(
          //               backgroundColor: Theme.of(context).secondaryHeaderColor,
          //               radius: 4,
          //             ),
          //             SizedBox(
          //               width: 10,
          //             ),
          //             Text('Астрофизика',style: GoogleFonts.openSans(
          //                 fontSize: 14,
          //                 fontWeight: FontWeight.w400,
          //                 color: Color(0xff595959)
          //             ),)
          //           ],
          //         ),
          //       ),
          //       Container(
          //         margin: EdgeInsets.only(bottom: 10),
          //         child: Row(
          //           children: [
          //             CircleAvatar(
          //               backgroundColor: Theme.of(context).secondaryHeaderColor,
          //               radius: 4,
          //             ),
          //             SizedBox(
          //               width: 10,
          //             ),
          //             Text('Астрофизика',style: GoogleFonts.openSans(
          //                 fontSize: 14,
          //                 fontWeight: FontWeight.w400,
          //                 color: Color(0xff595959)
          //             ),)
          //           ],
          //         ),
          //       ),
          //
          //     ],
          //   ),
          // ) : SizedBox.shrink(),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(bottom: 10),
            child: RichText(
                text: TextSpan(
                    text: 'Цена:  ',
                    style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Colors.black),
                    children: [
                  TextSpan(
                      text: '4200 тг',
                      style: GoogleFonts.openSans(
                          fontSize: 16, fontWeight: FontWeight.w600)),
                  TextSpan(text: '/ в месяц')
                ])),
          ),
          Container(
            width: MediaQuery.of(context).size.width - 32,
            child: RaisedButton(
              onPressed: () => {},
              child: Text(
                'Подписка оформлена',
                style: GoogleFonts.openSans(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
              color: Theme.of(context).secondaryHeaderColor,
            ),
          )
        ],
      ),
    );
  }
}
