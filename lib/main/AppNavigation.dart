import 'package:consulting/catalog/Catalog.dart';
import 'package:consulting/chat/index.dart';
import 'package:consulting/main/MainPage.dart';
import 'package:consulting/profile/index.dart';
import 'package:consulting/services/notification.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class AppNavigation extends StatefulWidget {
  @override
  _AppNavigationState createState() => _AppNavigationState();
}

class _AppNavigationState extends State<AppNavigation> {
  List<Widget> _buildScreens() {
    return [
      MainPage(),
      CatalogMain(),
      ChatIndex(
        isConsultant: false,
      ),
      ProfileIndex(),
    ];
  }

  PersistentTabController _controller;

  @override
  void initState() {
    // TODO: implement initState
    FirebaseService().init();

    super.initState();

    _controller = PersistentTabController(initialIndex: 0);
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
          icon: Icon(Icons.widgets),
          title: "Главная",
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.grey),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.format_list_bulleted),
          title: ("Каталог"),
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.grey),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.message),
          title: ("Чат"),
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.grey),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.person),
        title: ("Профиль"),
        activeColorPrimary: Colors.blue,
        inactiveColorPrimary: Colors.grey,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(),
      items: _navBarsItems(),
      backgroundColor: Colors.white,
      handleAndroidBackButtonPress: true,
      resizeToAvoidBottomInset: true,
      onItemSelected: (int) {
        setState(
            () {}); // This is required to update the nav bar if Android back button is pressed
      },
      stateManagement: true,
      hideNavigationBarWhenKeyboardShows: true,
      decoration: NavBarDecoration(borderRadius: BorderRadius.circular(0)),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      navBarStyle: NavBarStyle.style6,
    );
  }
}
