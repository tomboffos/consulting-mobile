import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:consulting/auth/Start.dart';
import 'package:consulting/auth/client/pincode.dart';
import 'package:consulting/auth/client/register.dart';
import 'package:consulting/catalog/Catalog.dart';
import 'package:consulting/consultants/ConsultantPage.dart';
import 'package:consulting/consultants/ConsultantsCatalog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  CarouselController _carouselController = CarouselController();
  List<dynamic> categories = [];
  List<dynamic> banners = [];
  bool loading = false;

  fetchBanners() async {
    setState(() {
      loading = true;
    });
    final response = await get(Uri.parse('$apiEndPoint/banners'));
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      setState(() {
        loading = false;
        banners = body['data'];
      });
    }
  }

  fetchCategories() async {
    setState(() {
      loading = true;
    });
    final response = await get(Uri.parse('$apiEndPoint/category'));
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      setState(() {
        loading = false;
        categories = body['data'];
      });
    }
  }

  Map<String, dynamic> user;

  checkToken() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    if (token != null) {
      final response = await get(Uri.parse('$apiEndPoint/user'),
          headers: {"Authorization": "Bearer $token"});
      final body = jsonDecode(response.body);
      setState(() {
        user = body['data'];
      });
    }
  }

  List<dynamic> profiles = [];
  fetchProfiles() async {
    try {
      final response = await get(Uri.parse('$apiEndPoint/profiles'));
      setState(() {
        profiles = jsonDecode(response.body)['data'];
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchCategories();
    fetchBanners();
    fetchProfiles();
    checkToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(
                  top: 50,
                ),
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () => pushNewScreen(context,
                          screen: StartScreen(), withNavBar: false),
                      child: Container(
                        padding: EdgeInsets.only(left: 12, right: 12),
                        margin: EdgeInsets.only(bottom: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  user != null
                                      ? user['name']
                                      : 'Зарегестрироваться',
                                  style: GoogleFonts.openSans(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xff2CB819)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'Консультируй и получай консультации онлайн',
                                  style: GoogleFonts.openSans(
                                      fontSize: 11,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black),
                                )
                              ],
                            ),
                            CircleAvatar(
                              radius: 23,
                              backgroundColor: Colors.grey,
                              foregroundImage: user == null
                                  ? NetworkImage(
                                      'https://personal-lawyer.a-lux.dev/storage/users/default.png')
                                  : NetworkImage(user['avatar'] != null
                                      ? user['avatar']
                                      : 'https://personal-lawyer.a-lux.dev/storage/users/default.png'),
                            )
                          ],
                        ),
                      ),
                    ),
                    // Container(
                    //   padding: EdgeInsets.only(left: 12, right: 12, bottom: 20),
                    //   child: TextField(
                    //     style: GoogleFonts.roboto(
                    //         fontWeight: FontWeight.w400,
                    //         fontSize: 16,
                    //         color: Colors.white),
                    //     decoration: InputDecoration(
                    //       fillColor: Theme.of(context).primaryColorDark,
                    //       filled: true,
                    //       prefixIcon: Icon(
                    //         Icons.search,
                    //         color: Colors.white,
                    //       ),
                    //       hintText: 'Что вас интересует?',
                    //       hintStyle: GoogleFonts.roboto(
                    //           fontWeight: FontWeight.w400,
                    //           fontSize: 16,
                    //           color: Colors.white),
                    //       enabledBorder: OutlineInputBorder(
                    //           gapPadding: 2,
                    //           borderRadius: BorderRadius.circular(100),
                    //           borderSide: new BorderSide(
                    //               color: Theme.of(context).primaryColorDark,
                    //               width: 1.0)),
                    //       focusedBorder: OutlineInputBorder(
                    //           gapPadding: 2,
                    //           borderRadius: BorderRadius.circular(100),
                    //           borderSide: new BorderSide(
                    //               color: Theme.of(context).primaryColorDark,
                    //               width: 1.0)),
                    //     ),
                    //   ),
                    // ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 12, right: 12, bottom: 19),
                      child: Text(
                        'Вас могут заинтересовать!',
                        style: GoogleFonts.openSans(
                            fontSize: 17,
                            fontWeight: FontWeight.w400,
                            color: Colors.black),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 10, bottom: 30),
                        child: CarouselSlider(
                          carouselController: _carouselController,
                          options: CarouselOptions(
                            aspectRatio: 10 / 5,
                            enableInfiniteScroll: true,
                            enlargeStrategy: CenterPageEnlargeStrategy.height,
                          ),
                          items: banners
                              .map((item) => GestureDetector(
                                    onTap: () => launch(item['link']),
                                    child: Container(
                                      width: 350,
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 5),
                                      padding:
                                          EdgeInsets.only(left: 20, top: 20),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [],
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image:
                                                  NetworkImage(item['image']))),
                                    ),
                                  ))
                              .toList(),
                        )),
                    Container(
                      padding: EdgeInsets.only(left: 12, right: 12),
                      margin: EdgeInsets.only(top: 16, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Топ категории',
                            style: GoogleFonts.roboto(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff303030)),
                          ),
                          GestureDetector(
                            onTap: () =>
                                pushNewScreen(context, screen: CatalogMain()),
                            child: Text(
                              'Посмотреть все',
                              style: GoogleFonts.roboto(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff55CB8E)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 12),
                      height: 250,
                      margin: EdgeInsets.only(top: 10, bottom: 18),
                      child: GridView.count(
                        scrollDirection: Axis.horizontal,
                        crossAxisCount: 2,
                        children: categories
                            .map((slider) => GestureDetector(
                                  onTap: () => pushNewScreen(context,
                                      screen: ConsultantsCatalog(
                                        category: slider,
                                      )),
                                  child: Container(
                                    margin:
                                        EdgeInsets.only(right: 12, bottom: 4),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Color(0xffE7E7E7)),
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          height: 90,
                                          margin: EdgeInsets.only(bottom: 4),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            image: DecorationImage(
                                                image: NetworkImage(
                                                    slider['image']),
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                        Text(
                                          slider['name'],
                                          style: GoogleFonts.openSans(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w700),
                                        )
                                      ],
                                    ),
                                  ),
                                ))
                            .toList(),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 12, right: 12),
                      margin: EdgeInsets.only(top: 16, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Лучшие консультанты',
                            style: GoogleFonts.roboto(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff303030)),
                          ),
                          GestureDetector(
                            onTap: () =>
                                pushNewScreen(context, screen: CatalogMain()),
                            child: Text(
                              'Посмотреть все',
                              style: GoogleFonts.roboto(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff55CB8E)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 380,
                      margin: EdgeInsets.only(top: 20, left: 12, bottom: 18),
                      child: GridView.builder(
                          scrollDirection: Axis.horizontal,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            crossAxisSpacing: 0.0,
                            mainAxisSpacing: 0.0,
                          ),
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ConsultantPage(
                                            userId: profiles[index]['user']
                                                ['id'],
                                          ))),
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color(0xffBBBBBB), width: 0.5)),
                                padding: EdgeInsets.only(
                                    left: 4, right: 4, bottom: 4, top: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Center(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: CircleAvatar(
                                              foregroundImage: NetworkImage(
                                                  '${profiles[index]['user']['avatar']}'),
                                              backgroundColor: Colors.grey,
                                              radius: 30,
                                            ),
                                            margin: EdgeInsets.only(bottom: 10),
                                          ),
                                          Text(
                                            '${profiles[index]['user']['name']}',
                                            style: GoogleFonts.openSans(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.black),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(top: 5),
                                              child: Row(
                                                children: [
                                                  Icon(
                                                    Icons.star,
                                                    color: profiles[index]
                                                                    ['user']
                                                                ['rating'] <=
                                                            1
                                                        ? Color(0xff989898)
                                                        : Color(0xff55CB8E),
                                                    size: 17,
                                                  ),
                                                  Icon(
                                                    Icons.star,
                                                    color: profiles[index]
                                                                    ['user']
                                                                ['rating'] <=
                                                            2
                                                        ? Color(0xff989898)
                                                        : Color(0xff55CB8E),
                                                    size: 17,
                                                  ),
                                                  Icon(
                                                    Icons.star,
                                                    color: profiles[index]
                                                                    ['user']
                                                                ['rating'] <=
                                                            3
                                                        ? Color(0xff989898)
                                                        : Color(0xff55CB8E),
                                                    size: 17,
                                                  ),
                                                  Icon(
                                                    Icons.star,
                                                    color: profiles[index]
                                                                    ['user']
                                                                ['rating'] <=
                                                            4
                                                        ? Color(0xff989898)
                                                        : Color(0xff55CB8E),
                                                    size: 17,
                                                  ),
                                                  Icon(
                                                    Icons.star,
                                                    color: profiles[index]
                                                                    ['user']
                                                                ['rating'] <=
                                                            5
                                                        ? Color(0xff989898)
                                                        : Color(0xff55CB8E),
                                                    size: 17,
                                                  )
                                                ],
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                              )),
                                          Container(
                                            child: Text(
                                              '${profiles[index]['user']['reviews'].length} отзывов',
                                              style: GoogleFonts.openSans(
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xff1D9CDD)),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                              'Профессия: ${profiles[index]['category']['name']}',
                                              style: GoogleFonts.openSans(
                                                  fontSize: 11,
                                                  fontWeight: FontWeight.w400),
                                              textAlign: TextAlign.center,
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                          itemCount: profiles.length),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 20,
                      padding: EdgeInsets.symmetric(horizontal: 47),
                      margin: EdgeInsets.only(bottom: 18),
                      child: RaisedButton(
                        onPressed: () =>
                            pushNewScreen(context, screen: CatalogMain()),
                        color: Theme.of(context).primaryColorDark,
                        child: Text(
                          'Получить консультацию',
                          style: GoogleFonts.roboto(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Colors.white),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(100)),
                        padding: EdgeInsets.symmetric(vertical: 16),
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
