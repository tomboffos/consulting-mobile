import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/consultants/ConsultantItem.dart';
import 'package:consulting/filter/index.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConsultantsCatalog extends StatefulWidget {
  final category;

  const ConsultantsCatalog({Key key, this.category}) : super(key: key);
  @override
  _ConsultantsCatalogState createState() => _ConsultantsCatalogState();
}

class _ConsultantsCatalogState extends State<ConsultantsCatalog> {
  List<dynamic> profiles = [];
  Map<String, dynamic> filter = {};

  fetchProfiles() async {
    try {
      final prices = (await SharedPreferences.getInstance()).get('prices');
      final times = (await SharedPreferences.getInstance()).get('times');
      final ratings = (await SharedPreferences.getInstance()).get('ratings');
      if (times != null) {
        filter['times'] = times;
      } else {
        if (filter.containsKey('times')) filter.remove('times');
      }
      if (prices != null) {
        filter['prices'] = prices;
      } else {
        if (filter.containsKey('prices')) filter.remove('prices');
      }

      if (ratings != null) {
        filter['ratings'] = 1;
      } else {
        if (filter.containsKey('ratings')) filter.remove('ratings');
      }

      print(filter);
      final response = await post(
          Uri.parse('$apiEndPoint/consultants/${widget.category['id']}'),
          headers: {
            "Accept": "application/json",
            "Content-type": "application/json"
          },
          body: json.encode(filter));
      final body = jsonDecode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          profiles = body['data'];
        });
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchProfiles();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${widget.category['name']}',
          style: GoogleFonts.openSans(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        actions: [
          Container(
            padding: EdgeInsets.only(right: 10),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context,
                        MaterialPageRoute(builder: (context) => FilterIndex()))
                    .then((value) {
                  setState(() {
                    fetchProfiles();
                  });
                });
              },
              child: Icon(Icons.tune),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: RefreshIndicator(
        onRefresh: () => fetchProfiles(),
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Container(
            margin: EdgeInsets.only(top: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          '${widget.category['name']}',
                          style: GoogleFonts.openSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text(
                          '${profiles.length} консультантов',
                          style: GoogleFonts.openSans(
                              fontSize: 14, fontWeight: FontWeight.w400),
                        ),
                      )
                    ],
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) => ConsultantItem(
                    profile: profiles[index],
                  ),
                  itemCount: profiles.length,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
