import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/consultants/tabs/Questionnaire.dart';
import 'package:consulting/consultants/tabs/Reviews.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';

class ConsultantPage extends StatefulWidget {
  final userId;

  const ConsultantPage({Key key, this.userId}) : super(key: key);
  @override
  _ConsultantPageState createState() => _ConsultantPageState();
}

class _ConsultantPageState extends State<ConsultantPage> {
  Map<String,dynamic> user;
  bool loading = false;
  fetchUserAccount()async{
    setState(() {
      loading = true;
    });
    final response = await get(Uri.parse('$apiEndPoint/consultants/profiles/${widget.userId}'));
    final body = jsonDecode(response.body);

    if(response.statusCode == 200){
      setState(() {
        loading = false;
        user = body['data'];
      });
    }
  }
  @override
  void initState() {
    fetchUserAccount();
    // TODO: implement initState
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Профиль преподователя',
            style: GoogleFonts.openSans(
                fontSize: 18,
                fontWeight: FontWeight.w700,
                color: Color(0xff191919)),
          ),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Color(0xff191919)),
          elevation: 0,
        ),
        backgroundColor: Colors.white,
        body: loading ? Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Center(child: CircularProgressIndicator()),
        ) : SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Container(
            margin: EdgeInsets.only(top: 12),
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    CircleAvatar(
                      foregroundImage: user['avatar'] == null ? NetworkImage('https://personal-lawyer.a-lux.dev/storage/users/default.png') : NetworkImage(user['avatar']),
                      radius: 31,
                      backgroundColor: Colors.grey,
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${user['name']}',
                            style: GoogleFonts.openSans(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Color(0xff0E0E0E)),
                          ),
                          // Container(
                          //   margin: EdgeInsets.only(top: 2),
                          //   child: Text(
                          //     'Онлайн',
                          //     style: GoogleFonts.openSans(
                          //         fontSize: 14,
                          //         fontWeight: FontWeight.w400,
                          //         color: Color(0xff6C6C6C)),
                          //   ),
                          // ),
                          Container(
                              margin: EdgeInsets.only(top: 5),
                              child: Row(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.star,
                                        color: user['rating'] <= 1 ? Color(0xff989898) : Color(0xff55CB8E),
                                        size: 17,
                                      ),
                                      Icon(
                                        Icons.star,
                                        color: user['rating'] <= 2 ? Color(0xff989898) : Color(0xff55CB8E),
                                        size: 17,
                                      ),
                                      Icon(
                                        Icons.star,
                                        color: user['rating'] <= 3 ? Color(0xff989898) : Color(0xff55CB8E),
                                        size: 17,
                                      ),
                                      Icon(
                                        Icons.star,
                                        color: user['rating'] <= 4 ? Color(0xff989898) : Color(0xff55CB8E),
                                        size: 17,
                                      ),
                                      Icon(
                                        Icons.star,
                                        color: user['rating'] <= 5 ? Color(0xff989898) : Color(0xff55CB8E),
                                        size: 17,
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Text(
                                    '${user['reviews'].length} отзывов',
                                    style: GoogleFonts.openSans(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400,
                                        color:
                                            Theme.of(context).primaryColor),
                                  )
                                ],
                              )),
                        ],
                      ),
                    )
                  ],
                ),
                // Container(
                //   margin: EdgeInsets.only(
                //     top: 13,
                //   ),
                //   child: RichText(
                //     text: TextSpan(
                //         text: 'Профессия: ',
                //         style: GoogleFonts.openSans(
                //             fontSize: 13,
                //             fontWeight: FontWeight.w600,
                //             color: Colors.black),
                //         children: [
                //           TextSpan(
                //               text:
                //                   ' математик, физик, астрофизик, атомный ядерщик',
                //               style: GoogleFonts.openSans(
                //                   fontWeight: FontWeight.w400))
                //         ]),
                //   ),
                // ),
                SizedBox(
                  height: 10,
                ),
                Divider(
                  color: Color(0xff4E4E4E),
                ),
                TabBar(
                    labelColor: Colors.black,
                    labelStyle: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff0E0E0E)),
                    unselectedLabelColor: Colors.black,
                    indicatorColor: Color(0xff1D9CDD),
                    isScrollable: true,
                    unselectedLabelStyle: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff0E0E0E)),
                    tabs: [
                      Tab(
                        child: Row(
                          children: [
                            Text(
                              'Анкеты',
                            ),
                            SizedBox(width: 5,),
                            Container(
                              padding: EdgeInsets.all(2),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.black
                                )
                              ),
                              child: Text('${user['profiles'].length}',style: GoogleFonts.openSans(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Color(0xff0E0E0E)
                              ),),
                            )
                          ],
                        ),

                      ),
                      Tab(
                        child: Row(
                          children: [
                            Text(
                              'Отзывы',
                            ),
                            SizedBox(width: 5,),
                            Container(
                              padding: EdgeInsets.all(2),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black
                                  )
                              ),
                              child: Text('${user['reviews'].length}',style: GoogleFonts.openSans(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12,
                                  color: Color(0xff0E0E0E)
                              ),),
                            )
                          ],
                        ),

                      )
                    ]),
                Container(
                    height: height,
                    child:
                        TabBarView(children: [Questionnaire(profiles: user['profiles'],), Reviews(reviews: user['reviews'],)])),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
