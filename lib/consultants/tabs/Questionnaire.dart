import 'package:consulting/consultants/tabs/questionary/QuestionaryItem.dart';
import 'package:flutter/material.dart';

class Questionnaire extends StatefulWidget {
  final profiles;

  const Questionnaire({Key key, this.profiles}) : super(key: key);
  @override
  _QuestionnaireState createState() => _QuestionnaireState();
}

class _QuestionnaireState extends State<Questionnaire> {
  @override
  void initState() {
    // TODO: implement initState
    print(widget.profiles);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: ListView.builder(
        itemBuilder: (context,index)=>QuestionnaryItem(profile: widget.profiles[index],),
        itemCount: widget.profiles.length,
      ),
    );
  }
}
