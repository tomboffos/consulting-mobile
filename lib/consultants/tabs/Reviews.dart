import 'package:consulting/consultants/tabs/review/ReviewItem.dart';
import 'package:consulting/review/CreateReview.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class Reviews extends StatefulWidget {
  final reviews;

  const Reviews({Key key, this.reviews}) : super(key: key);

  @override
  _ReviewsState createState() => _ReviewsState();
}

class _ReviewsState extends State<Reviews> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        children: [
          Container(
            width: width - 32,
            margin: EdgeInsets.only(bottom: 16),
            child: RaisedButton(
              color: Colors.white,
              onPressed: () => {
                pushNewScreen(
                  context,
                  screen: CreateReview(),
                  withNavBar: false, // OPTIONAL VALUE. True by default.
                  pageTransitionAnimation: PageTransitionAnimation.cupertino,
                )
              },
              padding: EdgeInsets.symmetric(vertical: 14),
              shape: Border.all(color: Theme.of(context).primaryColor),
              child: Text(
                'Написать отзыв',
                style: GoogleFonts.openSans(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Theme.of(context).primaryColor),
              ),
            ),
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) => ReviewItem(review: widget.reviews[index],),
            itemCount: widget.reviews.length,
          )
        ],
      ),
    );
  }
}
