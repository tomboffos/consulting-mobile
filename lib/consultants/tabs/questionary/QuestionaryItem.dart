import 'dart:convert';

import 'package:consulting/auth/client/register.dart';
import 'package:consulting/components/modals.dart';
import 'package:consulting/payments/payment_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionnaryItem extends StatefulWidget {
  final profile;

  const QuestionnaryItem({Key key, this.profile}) : super(key: key);

  @override
  _QuestionnaryItemState createState() => _QuestionnaryItemState();
}

class _QuestionnaryItemState extends State<QuestionnaryItem> {
  bool showTimes;
  bool mainValue;

  DateTime startDay;
  DateTime endDay;
  int hourDiffer;

  void showDays() {
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime.now(),
      maxTime: DateTime.now().add(Duration(days: 60)),
      theme: DatePickerTheme(
          itemStyle: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w400, fontSize: 18),
          doneStyle: TextStyle(color: Colors.black, fontSize: 16)),
      onConfirm: (date) {
        showTime(date);
      },
      currentTime: DateTime.now(),
      locale: LocaleType.ru,
    );
  }

  void clearDays() {
    setState(() {
      startDay = null;
      endDay = null;
      hourDiffer = null;
    });
  }

  int price;

  payConsultation() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await post(
        Uri.parse(
          '$apiEndPoint/user/consultation',
        ),
        headers: {
          "Authorization": "Bearer $token",
          "Accept": "application/json"
        },
        body: {
          "profile_id": widget.profile['id'].toString(),
          "start_time": startDay.toString(),
          "end_time": endDay.toString(),
          "price": price.toString()
        });
    print(response.body);
    if (response.statusCode == 422) {
      final body = jsonDecode(response.body);

      String message = '';
      body['errors'].forEach((k, v) => message += '\n ${v[0]}');
      showErrorModal(context, message);
    } else if (response.statusCode == 400) {
      final body = jsonDecode(response.body);

      showErrorModal(context, body['message']);
    } else
      pushNewScreen(context,
          screen: PaymentPage(
            data: response.body,
          ));
  }

  void showTime(date) {
    DatePicker.showTimePicker(context, showTitleActions: true,
        onConfirm: (time) {
      setState(() {
        startDay = new DateTime(
            date.year, date.month, date.day, time.hour, time.minute);
      });
      showEndTime(time);
    },
        currentTime: DateTime.now(),
        locale: LocaleType.ru,
        showSecondsColumn: false);
  }

  void showEndTime(date) {
    DatePicker.showTimePicker(context, showTitleActions: true,
        onConfirm: (time) {
      setState(() {
        endDay = new DateTime(startDay.year, startDay.month, startDay.day,
            time.hour, time.minute);
        hourDiffer = endDay.difference(startDay).inHours;
        price = widget.profile['price'] * hourDiffer;
        print(price);
      });
    },
        currentTime: startDay.add(Duration(hours: 1)),
        locale: LocaleType.ru,
        showSecondsColumn: false);
  }

  @override
  void initState() {
    // TODO: implement initState
    showTimes = false;
    mainValue = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                children: [
                  CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 3,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  RichText(
                      text: TextSpan(
                          text: 'Профессия: ',
                          style: GoogleFonts.openSans(
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                              color: Colors.black),
                          children: [
                        TextSpan(
                            text: '${widget.profile['category']['name']}',
                            style: GoogleFonts.openSans(
                                fontWeight: FontWeight.w400))
                      ])),
                ],
              ),
              SizedBox(
                width: 18,
              ),
              Row(
                children: [
                  CircleAvatar(
                    backgroundColor: Theme.of(context).secondaryHeaderColor,
                    radius: 3,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  RichText(
                      text: TextSpan(
                          text: 'Начал работу с: ',
                          style: GoogleFonts.openSans(
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                              color: Colors.black),
                          children: [
                        TextSpan(
                            text: '${widget.profile['start_date']}',
                            style: GoogleFonts.openSans(
                                fontWeight: FontWeight.w400))
                      ])),
                ],
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 4),
            child: Text(
              'Описание',
              style: GoogleFonts.openSans(
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff1F1F1F)),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 4, bottom: 14),
            child: Text(
              widget.profile['description'],
              style: GoogleFonts.openSans(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff434343)),
            ),
          ),
          //price
          startDay != null && endDay != null
              ? Container(
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '${startDay.toLocal()}',
                              style: GoogleFonts.openSans(fontSize: 15),
                            ),
                            Text(
                              '${endDay.toLocal()}',
                              style: GoogleFonts.openSans(fontSize: 15),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              : SizedBox.shrink(),
          Container(
            margin: EdgeInsets.only(top: 14, bottom: 10),
            child: RichText(
              text: TextSpan(
                  text: 'Цена: ',
                  style: GoogleFonts.openSans(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff191919)),
                  children: [
                    TextSpan(
                        text:
                            '${hourDiffer != null ? widget.profile['price'] * hourDiffer : widget.profile['price'].toString() + ' ' + widget.profile['currency']['symbol'] + '/час'} ',
                        style: GoogleFonts.openSans(
                            fontWeight: FontWeight.w600, fontSize: 18))
                  ]),
            ),
          ),
          //button
          Container(
            width: width - 32,
            margin: EdgeInsets.only(
              top: 12,
              bottom: 9,
            ),
            child: RaisedButton(
              onPressed: () => startDay != null && endDay != null
                  ? payConsultation()
                  : showDays(),
              color: Theme.of(context).primaryColor,
              child: Text(
                startDay != null && endDay != null
                    ? 'Записаться на выбранное время'
                    : 'Записаться на свободное время',
                style: GoogleFonts.openSans(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
              padding: EdgeInsets.symmetric(vertical: 15),
            ),
          ),
          startDay != null && endDay != null
              ? Container(
                  width: width - 32,
                  margin: EdgeInsets.only(
                    top: 12,
                    bottom: 9,
                  ),
                  child: RaisedButton(
                    onPressed: () => clearDays(),
                    color: Colors.grey,
                    child: Text(
                      'Отменить',
                      style: GoogleFonts.openSans(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 15),
                  ),
                )
              : SizedBox.shrink(),

          Divider(
            color: Color(0xff4E4E4E),
          )
        ],
      ),
    );
  }
}
