import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ReviewItem extends StatefulWidget {
  final review;

  const ReviewItem({Key key, this.review}) : super(key: key);
  @override
  _ReviewItemState createState() => _ReviewItemState();
}

class _ReviewItemState extends State<ReviewItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 5),
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 0),
              leading: CircleAvatar(
                backgroundColor: Colors.grey,
                radius: 14,
              ),
              title: Text(
                'Максим Ожидаев',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff0E0E0E)),
              ),
              minLeadingWidth: 24,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 0),
            child: Row(
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.star,
                      color: Color(0xff55CB8E),
                      size: 13,
                    ),
                    Icon(
                      Icons.star,
                      color: Color(0xff55CB8E),
                      size: 13,
                    ),
                    Icon(Icons.star, color: Color(0xff55CB8E), size: 13),
                    Icon(
                      Icons.star,
                      color: Color(0xff989898),
                      size: 13,
                    ),
                    Icon(
                      Icons.star,
                      color: Color(0xff989898),
                      size: 13,
                    )
                  ],
                ),
                SizedBox(
                  width: 12,
                ),
                Text(
                  '2 недели назад',
                  style: GoogleFonts.openSans(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff747474)),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 13),
            child: RichText(
              text: TextSpan(
                text:
                    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy ',
                style: GoogleFonts.openSans(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff434343)
                ),
                children: [
                  TextSpan(
                    text: 'ещё',
                    style: GoogleFonts.openSans(
                      fontWeight: FontWeight.w600,
                      color: Color(0xff1D9CDD),
                      decoration: TextDecoration.underline,
                    )
                  )
                ]
              ),
            ),
          )
        ],
      ),
    );
  }
}
