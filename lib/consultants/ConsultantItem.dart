import 'package:consulting/consultants/ConsultantPage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ConsultantItem extends StatelessWidget {
  final profile;

  const ConsultantItem({Key key, this.profile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => ConsultantPage(userId: profile['user']['id'],))),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
          leading: CircleAvatar(
            backgroundColor: Colors.grey,
            foregroundImage: NetworkImage(
                profile['user']['avatar'] == null ? 'https://personal-lawyer.a-lux.dev/storage/users/default.png' : profile['user']['avatar']),
          ),
          title: Text(
            '${profile['user']['name']}',
            style: GoogleFonts.openSans(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: Color(0xff040404)),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  RichText(
                      text: TextSpan(
                          text: 'Начал работу:',
                          style: GoogleFonts.openSans(
                            fontSize: 12,
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                          children: [
                        TextSpan(
                            text: '${profile['start_date']}',
                            style:
                                GoogleFonts.openSans(fontWeight: FontWeight.w400))
                      ])),
                  SizedBox(
                    width: 10,
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.star,
                        color:profile['user']['rating'] <= 1 ? Color(0xff989898) : Color(0xff55CB8E),
                        size: 15,
                      ),
                      Icon(
                        Icons.star,
                        color: profile['user']['rating'] <= 2 ? Color(0xff989898) : Color(0xff55CB8E),
                        size: 15,
                      ),
                      Icon(
                        Icons.star,
                        color: profile['user']['rating'] <= 3 ? Color(0xff989898) : Color(0xff55CB8E),
                        size: 15,
                      ),
                      Icon(
                        Icons.star,
                        color: profile['user']['rating'] <= 4 ? Color(0xff989898) : Color(0xff55CB8E),
                        size: 15,
                      ),
                      Icon(
                        Icons.star,
                        color: profile['user']['rating'] <= 5 ? Color(0xff989898) : Color(0xff55CB8E),
                        size: 15,
                      )
                    ],
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 3),
                child: RichText(
                    text: TextSpan(
                        text: 'Профессия: ',
                        style: GoogleFonts.openSans(
                            fontSize: 13,
                            fontWeight: FontWeight.w600,
                            color: Colors.black),
                        children: [
                      TextSpan(
                          text: '${profile['category']['name']}',
                          style: GoogleFonts.openSans(
                            fontWeight: FontWeight.w400,
                          ))
                    ])),
              )
            ],
          ),
          trailing: Text(
            '${profile['price']} ${profile['currency']['symbol']}',
            style: GoogleFonts.openSans(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: Color(0xff191919)),
          ),
          isThreeLine: true,
        ),
      ),
    );
  }
}
